# Worm_Simulator 🐛

_Worm_Simulator_ is a simulation of artificial life. It was made to study the relationship between artificial life and humans.

## The simulation

In the simulator there are some agents (artificial creatures similar to worms), that move in the enviroment in search of food.
Agents constantly lose energy. They can gain energy by eating food. When energy reaches 0 the agent dies.
When an agent dies it is immediatly replaced by a new one which is created according to an evolutionary scheme.
Every agent has a genotype that codes every genetic information of the worm.
When an agent dies another agent is selected according to a selection criteria, its genotype is selected and mutated and used to create the new agent.
Every worm has a set of sensors that make it possible for it to perceive the world around. 
The sensors are:
- Smell
- View
- Temperature
- Touch
- Human presence

Every creature has also a brain which is modeled with a MLP.

The simulator is also capable of interacting with people outside simulation. People can interact with the agents introducing food inside the simulation or kill one or more agents by clicking on them. People can also influence the simulation by just their presence in front of the screen. Thanks to a system of face detection made with `opencv` people can influence the artificial enviroment temperature which can be perceived by the worms sensors.

![Agents in the simulation](https://gitlab.com/step.lumumba/worm_simulator/-/raw/master/images/2022-03-12_12.21.54.jpg)

## How to use the software

The software is organized in java packages and classes representing the simulation. There are also some java libraries included.
The software supports different modes. The main distinction regards human presence:
- True human mode (real humans can interact with the system and the human presence influences the system)
- Fake human mode (a simulated fake human interacts with the software simulating human actions and human presence)
- No human mode (no humans interact with the simulation, the system is free to evolve)

The software can also be launched with or without the graphical render. There is also the option to run the simulation rendering "snapshots" of the simulation; note that in this way there can't be real human interaction because the rendering is not in real time.

Before launching there are some parameters that can be setted like:
- Number of worms in the simulation
- Selection criteria [_max age/distance from median age_] (⚠️ _distance from median age_ has problems, use _max age_)
- Loading of random worms or load existing worms
- Stop criterion (in simulated time)

The software can be launched by command line. By launching with the argument _gui_ a GUI that lets you set the parameters starts.
Otherwise you can directly pass the parameters you want as arguments. 

There are 9 arguments:
- Render mode (_norender/snapshot/realtime_)
- Selection criteria (_median/max_)
- File directory to save simulation data 
- File path to save simulation data in a .csv file
- Number of worms
- Human mode (_true/fake/nohuman_)
- Fake human behaviour (_bad/good_) [only in case of fake human mode, otherwise selection of this parameter is indifferent]
- Stop criteria (in simulated time)
- Creation of worms (_random_ for initial worms created with random genotypes, otherwise specify path of the .csv file where the genotypes of other worms are saved)

Once the simulation is launched there are a few keyboard keys that let you show and control some visual and functional features during the simulation. The most important are the following:
- P -> enables the face detection when in real time and real humans mode
- T -> shows the temperature of the simulated enviroment
- L -> shows the lidar sensor rays
- N -> shows the nose sensor

# More information

You can find more info in my master thesis [here](https://mega.nz/file/M3ADwazb#cUF6dfT4D9JgLrQ8Ae5-1G7vfb-BSp7utvx8K1gHNBY).
