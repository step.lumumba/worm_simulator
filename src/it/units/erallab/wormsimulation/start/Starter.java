package it.units.erallab.wormsimulation.start;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;

import com.opencsv.CSVWriter;

import it.units.erallab.wormsimulation.SimulationControl;

public class Starter {

    private static final String LIB_BIN = "it/units/erallab/wormsimulation/";

    private static String path = null;

    public static JFrame frame;

    private static void loadFromJar() {
        // we need to put both DLLs to temp dir
        String path = "temp";
        try {
            if (SystemUtils.IS_OS_WINDOWS) {
                if (System.getProperty("sun.arch.data.model").equals("64")) {
                    loadLib(path, "64/opencv_java452.dll");
                } else if (System.getProperty("sun.arch.data.model").equals("32")) {
                    loadLib(path, "32/opencv_java452.dll");
                }
            } else if (SystemUtils.IS_OS_LINUX) {
                loadLib(path, "so/libopencv_java453.so");
            } else if (SystemUtils.IS_OS_MAC) {
                loadLib(path, "so/libopencv_java455.dylib");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void loadLib(String path, String name) throws Exception {
        try {
            InputStream in = Starter.class.getClassLoader().getResourceAsStream(LIB_BIN + name);
            File fileOut = new File(System.getProperty("java.io.tmpdir") + "/" + path + LIB_BIN + name);
            System.out.println(LIB_BIN + name);
            OutputStream out = FileUtils.openOutputStream(fileOut);
            IOUtils.copy(in, out);
            in.close();
            out.close();
            System.load(fileOut.toString());
        } catch (Exception e) {
            throw new Exception("Failed to load required DLL", e);
        }
    }

    public static void main(String[] args) {
        // load library opencv for face detection
        try {
            System.out.println(System.getProperty("sun.arch.data.model"));
            System.out.println(System.getProperty("java.library.path"));
            loadFromJar();
        } catch (UnsatisfiedLinkError e) {
            loadFromJar();
        }
        // check on args[] input
        if (args[0].equals("gui")) {
            guiSimulation();
        } else {
            commandLineSimulation(args);
        }

    }

    private static void commandLineSimulation(String[] args) {
        if (args.length != 9) {
            printInputError();
        } else if (!args[5].equals("true") && !args[5].equals("fake") && !args[5].equals("nohuman")) {
            printInputError();
        } else {
            List<File> jsonFilez = new ArrayList<>();
            setCsvFiles(args);
            try {
                writeCsvHeadersToFiles();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            if (!args[8].equals("random")) {
                jsonFilez.add(new File(args[8]));
            }
            boolean foodAfterDeath = true;
            SimulationControl.filePathGeno = args[2];
            setSimulationMode(args, jsonFilez, foodAfterDeath);
        }
    }

    private static void setSimulationMode(String[] args, List<File> jsonFilez, boolean foodAfterDeath) {
        if (args[0].equals("norender") && args[1].equals("median")) {
            noRenderSimulation(SimulationControl.FitnessFunction.DISTANCEFORMMEDIANAGE,
                    Integer.parseInt(args[4]), jsonFilez, args[5], args[6], Long.parseLong(args[7]),
                    foodAfterDeath);

        } else if (args[0].equals("norender") && args[1].equals("max")) {
            noRenderSimulation(SimulationControl.FitnessFunction.MAXAGE, Integer.parseInt(args[4]), jsonFilez,
                    args[5], args[6], Long.parseLong(args[7]), foodAfterDeath);

        } else if (args[0].equals("snapshot") && args[1].equals("max")) {
            renderSnapshotFaster(SimulationControl.FitnessFunction.MAXAGE, Integer.parseInt(args[4]), jsonFilez,
                    args[5], args[6], Long.parseLong(args[7]), foodAfterDeath);

        } else if (args[0].equals("snapshot") && args[1].equals("median")) {
            renderSnapshotFaster(SimulationControl.FitnessFunction.DISTANCEFORMMEDIANAGE,
                    Integer.parseInt(args[4]), jsonFilez, args[5], args[6], Long.parseLong(args[7]),
                    foodAfterDeath);

        } else if (args[0].equals("realtime") && args[1].equals("median")) {
            renderRealTime(SimulationControl.FitnessFunction.DISTANCEFORMMEDIANAGE, Integer.parseInt(args[4]),
                    jsonFilez, args[5], args[6], Long.parseLong(args[7]), foodAfterDeath);

        } else if (args[0].equals("realtime") && args[1].equals("max")) {
            renderRealTime(SimulationControl.FitnessFunction.MAXAGE, Integer.parseInt(args[4]), jsonFilez,
                    args[5], args[6], Long.parseLong(args[7]), foodAfterDeath);

        } else {
            System.out.println("Input error");
        }
    }

    private static void writeCsvHeadersToFiles() throws IOException {
        SimulationControl.writer = new CSVWriter(new FileWriter(SimulationControl.fileToSave));
        String[] title = new String[]{"Time", "Age", "MaxDistanceX", "MaxDistanceY", "TotDistance",
                "MaxDistanceXlast100", "MaxDistanceYlast100", "TotDistanceLast100", "NumCircles", "Color",
                "NumFlagelli", "FlagelliLength"};
        SimulationControl.writer.writeNext(title);
        SimulationControl.writer.close();

        SimulationControl.writer2 = new CSVWriter(new FileWriter(SimulationControl.fileToSave2));
        String[] title2 = new String[]{"Time", "Age", "InitialX", "InitialY", "XMax", "YMax",
                "MaxDistanceX", "MaxDistanceY", "TotDistance", "MaxDistanceXlast100", "MaxDistanceYlast100",
                "TotDistanceLast100", "NumCircles", "Color", "NumFlagelli", "FlagelliLength", "Genotype",
                "Generation1", "Generation2"};
        SimulationControl.writer2.writeNext(title2);
        SimulationControl.writer2.close();

        SimulationControl.writer3 = new CSVWriter(new FileWriter(SimulationControl.fileToSave3));
        String[] title3 = new String[]{"Time", "Fake human"};
        SimulationControl.writer3.writeNext(title3);
        SimulationControl.writer3.close();

        SimulationControl.writer4 = new CSVWriter(new FileWriter(SimulationControl.fileToSave4));
        String[] title4 = new String[]{"SimulatedTime", "Time", "Xposition", "Yposition"};
        SimulationControl.writer4.writeNext(title4);
        SimulationControl.writer4.close();

        SimulationControl.writer5 = new CSVWriter(new FileWriter(SimulationControl.fileToSave5));
        String[] title5 = new String[]{"SimulatedTime", "Time", "Action", "Xposition", "Yposition",
                "ColorOfWorm", "NumberOfCircles", "NumberOfFlagels", "LengthOgFlagels"};
        SimulationControl.writer5.writeNext(title5);
        SimulationControl.writer5.close();
    }

    private static void printInputError() {
        System.out.println("Input error!");
        System.exit(0);
    }

    private static void setCsvFiles(String[] args) {
        SimulationControl.fileToSave = new File(args[3].substring(0, args[3].length() - 4).concat("-wormsAvg"));
        if (!FilenameUtils.getExtension(SimulationControl.fileToSave.getName()).equalsIgnoreCase("csv")) {
            SimulationControl.fileToSave = new File(SimulationControl.fileToSave.toString() + ".csv");
            SimulationControl.fileToSave = new File(SimulationControl.fileToSave.getParentFile(),
                    FilenameUtils.getBaseName(SimulationControl.fileToSave.getName()) + ".csv");
        }
        System.out.println("Save as file: " + SimulationControl.fileToSave.getAbsolutePath());

        String wormCsv = args[3].substring(0, args[3].length() - 4).concat("-worms");

        SimulationControl.fileToSave2 = new File(wormCsv);
        if (!FilenameUtils.getExtension(SimulationControl.fileToSave2.getName()).equalsIgnoreCase("csv")) {
            SimulationControl.fileToSave2 = new File(SimulationControl.fileToSave2.toString() + ".csv");
            SimulationControl.fileToSave2 = new File(SimulationControl.fileToSave2.getParentFile(),
                    FilenameUtils.getBaseName(SimulationControl.fileToSave2.getName()) + ".csv");
        }
        System.out.println("Save as file: " + SimulationControl.fileToSave2.getAbsolutePath());

        String fakeHumanCsv = args[3].substring(0, args[3].length() - 4).concat("-fakeHuman");
        SimulationControl.fileToSave3 = new File(fakeHumanCsv);
        if (!FilenameUtils.getExtension(SimulationControl.fileToSave3.getName()).equalsIgnoreCase("csv")) {
            SimulationControl.fileToSave3 = new File(SimulationControl.fileToSave3.toString() + ".csv");
            SimulationControl.fileToSave3 = new File(SimulationControl.fileToSave3.getParentFile(),
                    FilenameUtils.getBaseName(SimulationControl.fileToSave3.getName()) + ".csv");
        }
        System.out.println("Save as file: " + SimulationControl.fileToSave3.getAbsolutePath());

        String humanInterest = args[3].substring(0, args[3].length() - 4).concat("-humanInterest");
        SimulationControl.fileToSave4 = new File(humanInterest);
        if (!FilenameUtils.getExtension(SimulationControl.fileToSave4.getName()).equalsIgnoreCase("csv")) {
            SimulationControl.fileToSave4 = new File(SimulationControl.fileToSave4.toString() + ".csv");
            SimulationControl.fileToSave4 = new File(SimulationControl.fileToSave4.getParentFile(),
                    FilenameUtils.getBaseName(SimulationControl.fileToSave4.getName()) + ".csv");
        }
        System.out.println("Save as file: " + SimulationControl.fileToSave4.getAbsolutePath());

        String humanInteraction = args[3].substring(0, args[3].length() - 4).concat("-humanInteraction");
        SimulationControl.fileToSave5 = new File(humanInteraction);
        if (!FilenameUtils.getExtension(SimulationControl.fileToSave5.getName()).equalsIgnoreCase("csv")) {
            SimulationControl.fileToSave5 = new File(SimulationControl.fileToSave5.toString() + ".csv");
            SimulationControl.fileToSave5 = new File(SimulationControl.fileToSave5.getParentFile(),
                    FilenameUtils.getBaseName(SimulationControl.fileToSave5.getName()) + ".csv");
        }
        System.out.println("Save as file: " + SimulationControl.fileToSave5.getAbsolutePath());
    }

    public static void guiSimulation() {
        Color back = new Color(181, 178, 247);
        frame = new JFrame("Worm simulator");
        frame.getContentPane().setBackground(back);
        String[] numWorms = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
                "17", "18", "19", "20"};
        JComboBox<String> cb = new JComboBox<>(numWorms);
        cb.setBounds(450, 110, 100, 20);
        frame.add(cb);
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1100, 700);
        JLabel area1 = new JLabel();
        area1.setBounds(150, 90, 150, 10);
        area1.setText("Simulation mode:");
        frame.add(area1);
        JLabel area2 = new JLabel();
        area2.setBounds(150, 230, 150, 10);
        area2.setText("Selection criterion:");
        frame.add(area2);
        JLabel area3 = new JLabel();
        area3.setBounds(450, 90, 150, 10);
        area3.setText("Number of worms:");
        frame.add(area3);
        JLabel area4 = new JLabel();
        area4.setBounds(150, 320, 170, 20);
        area4.setText("Select folder for genotypes:");
        frame.add(area4);
        JButton buttonGen = new JButton("Select folder");
        buttonGen.setBounds(150, 350, 150, 19);
        buttonGen.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Select worm from JSON file");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int userSelection = fileChooser.showOpenDialog(frame);
            if (userSelection == JFileChooser.APPROVE_OPTION) {
                String file = fileChooser.getSelectedFile().toString();
                SimulationControl.filePathGeno = file;
                buttonGen.setText(file);
            }

        });
        frame.add(buttonGen);

        JLabel areaCsv = new JLabel();
        areaCsv.setBounds(150, 380, 170, 20);
        JLabel gen = new JLabel();
        gen.setBounds(450, 440, 200, 20);
        gen.setText("Stop criterion (simulated time): ");
        frame.add(gen);
        areaCsv.setText("Save simulation data in csv:");
        frame.add(areaCsv);
        JLabel foodAfterDeath = new JLabel();
        foodAfterDeath.setBounds(150, 450, 200, 19);
        foodAfterDeath.setText("Place food when a worm dies:");
        JButton buttonCsv = new JButton("Select folder");
        frame.add(foodAfterDeath);
        buttonCsv.setBounds(150, 410, 150, 19);
        buttonCsv.addActionListener(e -> {

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Save simulation data in a csv file");

            int userSelection = fileChooser.showSaveDialog(frame);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                String avg = fileChooser.getSelectedFile().toString().concat("-wormsAvg");
                SimulationControl.fileToSave = new File(avg);

                if (!FilenameUtils.getExtension(SimulationControl.fileToSave.getName()).equalsIgnoreCase("csv")) {
                    SimulationControl.fileToSave = new File(SimulationControl.fileToSave.toString() + ".csv");
                    SimulationControl.fileToSave = new File(SimulationControl.fileToSave.getParentFile(),
                            FilenameUtils.getBaseName(SimulationControl.fileToSave.getName()) + ".csv");
                }
                System.out.println("Save as file: " + SimulationControl.fileToSave.getAbsolutePath());
                String wormCsv = fileChooser.getSelectedFile().toString().concat("-worms");

                SimulationControl.fileToSave2 = new File(wormCsv);
                if (!FilenameUtils.getExtension(SimulationControl.fileToSave2.getName()).equalsIgnoreCase("csv")) {
                    SimulationControl.fileToSave2 = new File(SimulationControl.fileToSave2.toString() + ".csv");
                    SimulationControl.fileToSave2 = new File(SimulationControl.fileToSave2.getParentFile(),
                            FilenameUtils.getBaseName(SimulationControl.fileToSave2.getName()) + ".csv");
                }
                System.out.println("Save as file: " + SimulationControl.fileToSave2.getAbsolutePath());

                String fakeHumanCsv = fileChooser.getSelectedFile().toString().concat("-fakeHuman");
                SimulationControl.fileToSave3 = new File(fakeHumanCsv);
                if (!FilenameUtils.getExtension(SimulationControl.fileToSave3.getName()).equalsIgnoreCase("csv")) {
                    SimulationControl.fileToSave3 = new File(SimulationControl.fileToSave3.toString() + ".csv");
                    SimulationControl.fileToSave3 = new File(SimulationControl.fileToSave3.getParentFile(),
                            FilenameUtils.getBaseName(SimulationControl.fileToSave3.getName()) + ".csv");
                }
                System.out.println("Save as file: " + SimulationControl.fileToSave3.getAbsolutePath());

                String humanInterest = fileChooser.getSelectedFile().toString().concat("-humanInterest");
                SimulationControl.fileToSave4 = new File(humanInterest);
                if (!FilenameUtils.getExtension(SimulationControl.fileToSave4.getName()).equalsIgnoreCase("csv")) {
                    SimulationControl.fileToSave4 = new File(SimulationControl.fileToSave4.toString() + ".csv");
                    SimulationControl.fileToSave4 = new File(SimulationControl.fileToSave4.getParentFile(),
                            FilenameUtils.getBaseName(SimulationControl.fileToSave4.getName()) + ".csv");
                }
                System.out.println("Save as file: " + SimulationControl.fileToSave4.getAbsolutePath());

                String humanInteraction = fileChooser.getSelectedFile().toString().concat("-humanInteraction");
                SimulationControl.fileToSave5 = new File(humanInteraction);
                if (!FilenameUtils.getExtension(SimulationControl.fileToSave5.getName()).equalsIgnoreCase("csv")) {
                    SimulationControl.fileToSave5 = new File(SimulationControl.fileToSave5.toString() + ".csv");
                    SimulationControl.fileToSave5 = new File(SimulationControl.fileToSave5.getParentFile(),
                            FilenameUtils.getBaseName(SimulationControl.fileToSave5.getName()) + ".csv");
                }
                System.out.println("Save as file: " + SimulationControl.fileToSave5.getAbsolutePath());

            }
            try {
                SimulationControl.writer = new CSVWriter(new FileWriter(SimulationControl.fileToSave));
                String[] title = new String[]{"Time", "Age", "MaxDistanceX", "MaxDistanceY", "TotDistance",
                        "MaxDistanceXlast100", "MaxDistanceYlast100", "TotDistanceLast100", "NumCircles", "Color",
                        "NumFlagelli", "FlagelliLength"};
                SimulationControl.writer.writeNext(title);
                SimulationControl.writer.close();

                SimulationControl.writer2 = new CSVWriter(new FileWriter(SimulationControl.fileToSave2));
                String[] title2 = new String[]{"Time", "Age", "InitialX", "InitialY", "XMax", "YMax",
                        "MaxDistanceX", "MaxDistanceY", "TotDistance", "MaxDistanceXlast100", "MaxDistanceYlast100",
                        "TotDistanceLast100", "NumCircles", "Color", "NumFlagelli", "FlagelliLength", "Genotype",
                        "Generation1", "Generation2"};
                SimulationControl.writer2.writeNext(title2);
                SimulationControl.writer2.close();

                SimulationControl.writer3 = new CSVWriter(new FileWriter(SimulationControl.fileToSave3));
                String[] title3 = new String[]{"Time", "Fake human"};
                SimulationControl.writer3.writeNext(title3);
                SimulationControl.writer3.close();

                SimulationControl.writer4 = new CSVWriter(new FileWriter(SimulationControl.fileToSave4));
                String[] title4 = new String[]{"SimulatedTime", "Time", "Xposition", "Yposition"};
                SimulationControl.writer4.writeNext(title4);
                SimulationControl.writer4.close();

                SimulationControl.writer5 = new CSVWriter(new FileWriter(SimulationControl.fileToSave5));
                String[] title5 = new String[]{"SimulatedTime", "Time", "Action", "Xposition", "Yposition",
                        "ColorOfWorm", "NumberOfCircles", "NumberOfFlagels", "LengthOgFlagels"};
                SimulationControl.writer5.writeNext(title5);
                SimulationControl.writer5.close();

            } catch (IOException e1) {
                e1.printStackTrace();
            }

            buttonCsv.setText(SimulationControl.fileToSave.toString());
        });
        frame.add(buttonCsv);

        JRadioButton checkBox1 = new JRadioButton("No render simulation", true);
        checkBox1.setBounds(150, 100, 250, 50);
        checkBox1.setBackground(back);
        JRadioButton checkBox2 = new JRadioButton("Real time render", false);
        checkBox2.setBounds(150, 130, 250, 50);
        checkBox2.setBackground(back);
        JRadioButton checkBox3 = new JRadioButton("Render snapshot", false);
        checkBox3.setBounds(150, 160, 250, 50);
        checkBox3.setBackground(back);
        ButtonGroup simulationMode = new ButtonGroup();
        simulationMode.add(checkBox1);
        simulationMode.add(checkBox2);
        simulationMode.add(checkBox3);
        frame.add(checkBox1);
        frame.add(checkBox2);
        frame.add(checkBox3);

        JRadioButton checkFoodYes = new JRadioButton("Yes", true);
        checkFoodYes.setBounds(150, 480, 150, 19);
        checkFoodYes.setBackground(back);
        JRadioButton checkFoodNo = new JRadioButton("No", false);
        checkFoodNo.setBounds(150, 510, 150, 19);
        checkFoodNo.setBackground(back);
        ButtonGroup foodGroup = new ButtonGroup();
        foodGroup.add(checkFoodYes);
        foodGroup.add(checkFoodNo);
        frame.add(checkFoodYes);
        frame.add(checkFoodNo);

        JLabel scale = new JLabel();
        scale.setBounds(150, 540, 200, 19);
        scale.setText("Scale frame:");
        frame.add(scale);
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 10, 1);
        slider.setBounds(150, 570, 200, 50);
        slider.setMinorTickSpacing(1);
        slider.setMajorTickSpacing(10);
        slider.setValue(5);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        frame.add(slider);

        JRadioButton fakeHuman = new JRadioButton("Fake human interaction", true);
        fakeHuman.setBounds(450, 260, 200, 20);
        fakeHuman.setBackground(back);
        JRadioButton trueHuman = new JRadioButton("True human interaction", false);
        trueHuman.setBounds(450, 290, 200, 20);
        trueHuman.setBackground(back);
        JRadioButton noHuman = new JRadioButton("No human interaction", false);
        noHuman.setBounds(450, 320, 200, 20);
        noHuman.setBackground(back);
        ButtonGroup humanInteraction = new ButtonGroup();
        humanInteraction.add(fakeHuman);
        humanInteraction.add(trueHuman);
        humanInteraction.add(noHuman);
        frame.add(trueHuman);
        frame.add(fakeHuman);
        frame.add(noHuman);

        JSpinner spinner = new JSpinner(new SpinnerNumberModel(1, 1, 2000000000, 1));
        spinner.setBounds(450, 470, 120, 20);
        frame.add(spinner);

        JLabel fakeHumanBehaviour = new JLabel();
        fakeHumanBehaviour.setBounds(450, 350, 200, 20);
        fakeHumanBehaviour.setText("Fake human behaviour");
        frame.add(fakeHumanBehaviour);

        JRadioButton good = new JRadioButton("Good", true);
        good.setBounds(450, 380, 200, 20);
        good.setBackground(back);
        JRadioButton bad = new JRadioButton("Bad", false);
        bad.setBounds(450, 410, 200, 20);
        bad.setBackground(back);
        ButtonGroup behaviour = new ButtonGroup();
        behaviour.add(good);
        behaviour.add(bad);
        frame.add(bad);
        frame.add(good);

        JLabel ar = new JLabel();
        ar.setBounds(450, 230, 200, 20);
        ar.setText("Human interaction:");
        frame.add(ar);

        trueHuman.addActionListener(e -> {
            good.setEnabled(false);
            bad.setEnabled(false);
            fakeHumanBehaviour.setEnabled(false);

        });
        fakeHuman.addActionListener(e -> {
            good.setEnabled(true);
            bad.setEnabled(true);
            fakeHumanBehaviour.setEnabled(true);

        });
        noHuman.addActionListener(e -> {
            good.setEnabled(false);
            bad.setEnabled(false);
            fakeHumanBehaviour.setEnabled(false);

        });

        JRadioButton checkBoxWorms1 = new JRadioButton("Random worms", true);
        checkBoxWorms1.setBounds(450, 150, 200, 20);
        checkBoxWorms1.setBackground(back);
        JRadioButton checkBoxWorms2 = new JRadioButton("Load worms", false);
        checkBoxWorms2.setBounds(450, 180, 200, 20);
        checkBoxWorms2.setBackground(back);
        ButtonGroup worms = new ButtonGroup();
        worms.add(checkBoxWorms1);
        worms.add(checkBoxWorms2);
        frame.add(checkBoxWorms1);
        frame.add(checkBoxWorms2);

        JRadioButton check1 = new JRadioButton("Distance from median age", true);
        JRadioButton check2 = new JRadioButton("Max age", false);
        check1.setBounds(150, 240, 250, 50);
        check2.setBounds(150, 270, 250, 50);
        check1.setBackground(back);
        check2.setBackground(back);
        ButtonGroup fitnessFunction = new ButtonGroup();
        fitnessFunction.add(check1);
        fitnessFunction.add(check2);
        frame.add(check1);
        frame.add(check2);
        JButton button = new JButton("Launch simulation");
        button.setBounds(430, 520, 150, 30);
        frame.add(button);
        List<JButton> buttonList = new ArrayList<>();
        List<File> jsonFiles = new ArrayList<>();
        checkBoxWorms2.addActionListener(e -> {
            frame.repaint();
            for (JButton butt : buttonList) {
                frame.remove(butt);
            }
            buttonList.clear();
            jsonFiles.clear();

            JButton button1 = new JButton("Select file");
            button1.setBounds(650, 30, 150, 19);
            buttonList.add(button1);
            button1.addActionListener(e12 -> {

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Select worms from file");
                System.out.println(path);
                if (path != null) {
                    fileChooser.setCurrentDirectory(new File(path));
                }
                int userSelection = fileChooser.showOpenDialog(frame);
                if (userSelection == JFileChooser.APPROVE_OPTION) {

                    File file = fileChooser.getSelectedFile();
                    path = file.getParentFile().toString();
                    System.out.println(path);
                    jsonFiles.add(file);
                    button1.setBounds(button1.getX(), button1.getY(), 370, 19);
                    button1.setText(file.toString());
                }

            });
            frame.add(button1);

        });

        checkBoxWorms1.addActionListener(e -> {
            frame.repaint();
            for (JButton butt : buttonList) {
                frame.remove(butt);
            }
            buttonList.clear();
            jsonFiles.clear();

        });

        cb.addActionListener(e -> {
            if (checkBoxWorms2.isSelected()) {
                String nOfWorms = (String) cb.getSelectedItem();
                int increment = 0;
                frame.repaint();
                for (JButton butt : buttonList) {
                    frame.remove(butt);
                }
                buttonList.clear();
                jsonFiles.clear();
                JButton button12 = new JButton("Select file");
                button12.setBounds(650, 30, 150, 19);
                buttonList.add(button12);
                button12.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {

                        JFileChooser fileChooser = new JFileChooser();
                        fileChooser.setDialogTitle("Select worms from file");
                        int userSelection = fileChooser.showOpenDialog(frame);
                        if (userSelection == JFileChooser.APPROVE_OPTION) {
                            File file = fileChooser.getSelectedFile();
                            jsonFiles.add(file);
                            button12.setBounds(button12.getX(), button12.getY(), 370, 19);
                            button12.setText(file.toString());
                        }

                    }
                });
                frame.add(button12);

            }

        });
        button.addActionListener(e -> {
            try {
                if (buttonList.size() != jsonFiles.size() || (buttonList == null && jsonFiles == null)) {
                    throw new NullPointerException();
                }
                if (SimulationControl.filePathGeno.equals("") || SimulationControl.fileToSave == null
                        || SimulationControl.fileToSave2 == null) {
                    throw new IllegalArgumentException();
                }

                int numOfWorms = Integer.parseInt((String) cb.getSelectedItem());
                SimulationControl.scaleFrame = slider.getValue();
                if (checkBox1.isSelected() && check1.isSelected()) {
                    Thread thread = new Thread(() -> {
                        button.setText("running...");
                        button.setEnabled(false);
                        String human = "";
                        if (fakeHuman.isSelected()) {
                            human = "fake";
                        } else if (trueHuman.isSelected()) {
                            human = "true";
                        } else if (noHuman.isSelected()) {
                            human = "nohuman";
                        }
                        String behaviour1 = null;
                        if (good.isSelected()) {
                            behaviour1 = "good";
                        } else if (bad.isSelected()) {
                            behaviour1 = "bad";
                        }
                        boolean foodOnDeath = !checkFoodNo.isSelected();
                        noRenderSimulation(SimulationControl.FitnessFunction.DISTANCEFORMMEDIANAGE, numOfWorms,
                                jsonFiles, human, behaviour1, (Integer) spinner.getValue(), foodOnDeath);
                    });
                    thread.start();

                }
                if (checkBox1.isSelected() && check2.isSelected()) {
                    Thread thread = new Thread(() -> {
                        button.setText("running...");
                        button.setEnabled(false);
                        String human = "";
                        if (fakeHuman.isSelected()) {
                            human = "fake";
                        } else if (trueHuman.isSelected()) {
                            human = "true";
                        } else if (noHuman.isSelected()) {
                            human = "nohuman";
                        }
                        String behaviour1 = null;
                        if (good.isSelected()) {
                            behaviour1 = "good";
                        } else if (bad.isSelected()) {
                            behaviour1 = "bad";
                        }
                        boolean foodOnDeath = !checkFoodNo.isSelected();
                        noRenderSimulation(SimulationControl.FitnessFunction.MAXAGE, numOfWorms, jsonFiles,
                                human, behaviour1, (Integer) spinner.getValue(), foodOnDeath);
                    });
                    thread.start();

                }
                if (checkBox2.isSelected() && check1.isSelected()) {
                    Thread thread = new Thread(() -> {
                        button.setText("running...");
                        button.setEnabled(false);
                        String human = "";
                        if (fakeHuman.isSelected()) {
                            human = "fake";
                        } else if (trueHuman.isSelected()) {
                            human = "true";
                        } else if (noHuman.isSelected()) {
                            human = "nohuman";
                        }
                        String behaviour1 = null;
                        if (good.isSelected()) {
                            behaviour1 = "good";
                        } else if (bad.isSelected()) {
                            behaviour1 = "bad";
                        }
                        boolean foodOnDeath = !checkFoodNo.isSelected();
                        renderRealTime(SimulationControl.FitnessFunction.DISTANCEFORMMEDIANAGE, numOfWorms,
                                jsonFiles, human, behaviour1, (Integer) spinner.getValue(), foodOnDeath);
                    });
                    thread.start();

                }
                if (checkBox2.isSelected() && check2.isSelected()) {
                    Thread thread = new Thread(() -> {
                        button.setText("running...");
                        button.setEnabled(false);
                        String human = "";
                        if (fakeHuman.isSelected()) {
                            human = "fake";
                        } else if (trueHuman.isSelected()) {
                            human = "true";
                        } else if (noHuman.isSelected()) {
                            human = "nohuman";
                        }
                        String behaviour1 = null;
                        if (good.isSelected()) {
                            behaviour1 = "good";
                        } else if (bad.isSelected()) {
                            behaviour1 = "bad";
                        }
                        boolean foodOnDeath = !checkFoodNo.isSelected();
                        renderRealTime(SimulationControl.FitnessFunction.MAXAGE, numOfWorms, jsonFiles, human,
                                behaviour1, (Integer) spinner.getValue(), foodOnDeath);
                    });
                    thread.start();

                }
                if (checkBox3.isSelected() && check1.isSelected()) {
                    Thread thread = new Thread(() -> {
                        button.setText("running...");
                        button.setEnabled(false);
                        String human = "";
                        if (fakeHuman.isSelected()) {
                            human = "fake";
                        } else if (trueHuman.isSelected()) {
                            human = "true";
                        } else if (noHuman.isSelected()) {
                            human = "nohuman";
                        }
                        String behaviour1 = null;
                        if (good.isSelected()) {
                            behaviour1 = "good";
                        } else if (bad.isSelected()) {
                            behaviour1 = "bad";
                        }
                        boolean foodOnDeath = !checkFoodNo.isSelected();
                        renderSnapshotFaster(SimulationControl.FitnessFunction.DISTANCEFORMMEDIANAGE,
                                numOfWorms, jsonFiles, human, behaviour1, (Integer) spinner.getValue(),
                                foodOnDeath);
                    });
                    thread.start();

                }
                if (checkBox3.isSelected() && check2.isSelected()) {
                    Thread thread = new Thread(() -> {
                        button.setText("running...");
                        button.setEnabled(false);
                        String human = "";
                        if (fakeHuman.isSelected()) {
                            human = "fake";
                        } else if (trueHuman.isSelected()) {
                            human = "true";
                        } else if (noHuman.isSelected()) {
                            human = "nohuman";
                        }
                        String behaviour1 = null;
                        if (good.isSelected()) {
                            behaviour1 = "good";
                        } else if (bad.isSelected()) {
                            behaviour1 = "bad";
                        }
                        boolean foodOnDeath = !checkFoodNo.isSelected();
                        renderSnapshotFaster(SimulationControl.FitnessFunction.MAXAGE, numOfWorms, jsonFiles,
                                human, behaviour1, (Integer) spinner.getValue(), foodOnDeath);
                    });
                    thread.start();
                }
            } catch (NullPointerException ee) {
                JOptionPane.showMessageDialog(frame, "Insert all worms from JSON!");

            } catch (IllegalArgumentException e2) {
                JOptionPane.showMessageDialog(frame, "Some settings are missing to launch simulation");
            }
        });
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void renderRealTime(SimulationControl.FitnessFunction fitness, int numOfWorms, List<File> jsonFiles,
                                      String fakeHuman, String fakeHumanBehaviour, long stop, boolean foodOnDeath) {
        SimulationControl simulation = new SimulationControl(3, fitness, numOfWorms, jsonFiles, fakeHuman,
                fakeHumanBehaviour, stop, foodOnDeath);
        simulation.run();
    }

    public static void renderSnapshotFaster(SimulationControl.FitnessFunction fitness, int numOfWorms,
                                            List<File> jsonFiles, String fakeHuman, String fakeHumanBehaviour, long stop, boolean foodOnDeath) {
        SimulationControl simulation = new SimulationControl(2, fitness, numOfWorms, jsonFiles, fakeHuman,
                fakeHumanBehaviour, stop, foodOnDeath);
        simulation.run();
    }

    public static void noRenderSimulation(SimulationControl.FitnessFunction fitness, int numOfWorms,
                                          List<File> jsonFiles, String fakeHuman, String fakeHumanBehaviour, long stop, boolean foodOnDeath) {
        SimulationControl simulation = new SimulationControl(0, fitness, numOfWorms, jsonFiles, fakeHuman,
                fakeHumanBehaviour, stop, foodOnDeath);
        simulation.run();
    }

}
