package it.units.erallab.wormsimulation;

import java.awt.Toolkit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.dyn4j.geometry.Vector2;

public class FakeHuman {

    public static final double NANO_TO_BASE = 1.0e9;

    private final String movingFace;
    private int x;
    private int y;
    private final ScheduledExecutorService executor1;
    private final ScheduledExecutorService executor2;

    private double cycleStartTime;
    private double lastCycleDuration;

    private final int xMin = 160;
    private final int xMax = 310;
    private final int yMin = 70;
    private final int yMax = 170;
    private int diagonal = 0;
    private final double rate = 0.007;
    private final String behaviour;
    private boolean day;
    private long counterHuman;
    private long counterPause;
    private final double conversionRate;
    private int check = 0;

    public FakeHuman(String movingFace, String behaviour, int SimulationMode) {
        this.movingFace = movingFace;
        this.x = xMin;
        this.y = yMin;
        this.behaviour = behaviour;
        this.day = true;
        this.executor1 = Executors.newScheduledThreadPool(4);
        this.executor2 = Executors.newScheduledThreadPool(4);
        this.counterHuman = 0;
        this.counterPause = 0;
        cycleStartTime = System.nanoTime();
        if (SimulationMode == 3) {
            this.conversionRate = 1;
        } else if (SimulationMode == 1 || SimulationMode == 2) {
            this.conversionRate = 1.3;
        } else {
            this.conversionRate = 5.2;
        }
    }

    public void dd() {
        if (day) {
            SimulationControl.temperature.setMatrixValue(y, x, 50000);
            if (movingFace.equals("random")) {
                x = SimulationControl.random.nextInt(310 - 160) + 160;
                y = SimulationControl.random.nextInt(170 - 70) + 70;
            } else if (movingFace.equals("crossingRectangle")) {

                if (x == xMin && y == yMin) {
                    diagonal = 0;
                } else if (y == yMax && x != xMin) {
                    diagonal = 2;
                } else if (y == yMin && x != xMin) {
                    diagonal = 1;
                } else if (x == xMin && y <= yMax && y > yMin) {
                    diagonal = 2;
                    y--;
                }
                if (diagonal == 0) {
                    x++;
                    y++;
                }
                if (diagonal == 1) {
                    x--;
                    y++;
                }
                if (diagonal == 2) {
                    y--;
                }
            }
        }
        counterHuman++;
        counterPause++;

        if (counterHuman == 940 && day) {
            lastCycleDuration = (System.nanoTime() - cycleStartTime) / NANO_TO_BASE;
            cycleStartTime = System.nanoTime();
            switch (behaviour) {
                case "bad": {
                    int rnd = SimulationControl.random.nextInt(3);
                    for (int i = 0; i < rnd; i++) {
                        removeRandomWorm();
                    }
                    break;
                }
                case "good": {
                    int rnd = SimulationControl.random.nextInt(3);
                    for (int i = 0; i < rnd; i++) {
                        feedWorm();
                    }
                    break;
                }
                case "badColor": {
                    int rnd = SimulationControl.random.nextInt(3);
                    for (int i = 0; i < rnd; i++) {
                        removeColoredWorm(5);
                    }
                    break;
                }
            }
            day = false;
            counterHuman = 0;
            counterPause = 0;
            if (check == 0) {
                double[] toCsv = new double[2];
                toCsv[0] = (double) (System.nanoTime() - SimulationControl.startTime) / NANO_TO_BASE;
                toCsv[1] = 1;
                check = 1;
            }

        }
        if (counterPause == 20000 && !day) {
            day = true;
            counterPause = 0;
            counterHuman = 0;
            if (check == 1) {
                double[] toCsv = new double[2];
                toCsv[0] = (double) (System.nanoTime() - SimulationControl.startTime) / NANO_TO_BASE;
                toCsv[1] = 0;
                check = 0;
            }
        }

    }

    public void start() {
        Runnable facingHuman = this::dd;
        executor1.scheduleAtFixedRate(facingHuman, Math.round(1 * 1000d),
                Math.round(1000d / (15 * conversionRate)), TimeUnit.MILLISECONDS);

    }

    public static void feedWorm() {
        double width = 683.0;
        double height = 384.0;
        int selected = SimulationControl.random.nextInt(SimulationControl.worms.size());
        while (Math.abs(SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter().x) > width
                && Math.abs(SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter().y) > height) {
            selected = SimulationControl.random.nextInt(SimulationControl.worms.size());
        }
        SimulationControl.clickForFood = true;
        double direction = Math.PI * 2d + SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter()
                .subtract(SimulationControl.worms.get(selected).getBodies().get(1).getWorldCenter()).getDirection();
        Vector2 vec = new Vector2(direction);
        SimulationControl.pointClicked = SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter()
                .sum(vec.multiply(3));
    }

    public static void removeRandomWorm() {
        double width = 683.0;
        double height = 384.0;
        int selected = SimulationControl.random.nextInt(SimulationControl.worms.size());
        while (Math.abs(SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter().x) > width
                && Math.abs(SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter().y) > height) {
            selected = SimulationControl.random.nextInt(SimulationControl.worms.size());
        }
        SimulationControl.objectInteraction = SimulationControl.worms.get(selected).getBodies().get(0);

    }

    public static void removeColoredWorm(int color) {
        // Random random = new Random();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        double width = toolkit.getScreenSize().width / 2;
        double height = toolkit.getScreenSize().height / 2;

        int selected = SimulationControl.random.nextInt(SimulationControl.worms.size());
        while (Math.abs(SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter().x) > width
                && Math.abs(SimulationControl.worms.get(selected).getBodies().get(0).getWorldCenter().y) > height
                && Math.round(SimulationControl.worms.get(selected).getGenotype()[3]) == color) {
            selected = SimulationControl.random.nextInt(SimulationControl.worms.size());
        }
        SimulationControl.objectInteraction = SimulationControl.worms.get(selected).getBodies().get(0);

    }
}
