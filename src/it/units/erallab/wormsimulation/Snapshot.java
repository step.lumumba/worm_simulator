package it.units.erallab.wormsimulation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.rits.cloning.Cloner;
import org.dyn4j.collision.Bounds;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Ray;
import org.dyn4j.samples.framework.SimulationBody;
import org.dyn4j.world.World;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Snapshot implements Serializable {

    private final double time;
    private final Bounds bounds;
    private final int bodyCount;
    private final List<Color> bodyListColor;
    private final int jointCount;
    private final double getReactionForce;
    private final List<Worm> worms;
    private final double[][] temperatureMatrix;
    private final int wormsSize;
    private final List<List<BodyFixture>> listOfFixtureList;
    private final List<Double> getTranslationX;
    private final List<Double> getTranslationY;
    private final List<Double> getRotationAngle;

    @JsonCreator
    public Snapshot(@JsonProperty("time") double time, @JsonProperty("bounds") Bounds bounds,
                    @JsonProperty("bodyCount") int bodyCount, @JsonProperty("bodyListColor") List<Color> bodyListColor,
                    @JsonProperty("jointCount") int jointCount, @JsonProperty("getReactionforce") double getReactionForce,
                    @JsonProperty("worms") List<Worm> worms, @JsonProperty("temperatureMatrix") double[][] temperatureMatrix,
                    @JsonProperty("rayList") List<Ray> rayList, @JsonProperty("wormSize") int wormSize,
                    @JsonProperty("listOfFixtureList") List<List<BodyFixture>> listOfFixtureList,
                    @JsonProperty("getTranslationX") List<Double> getTranslationX,
                    @JsonProperty("getTranslationY") List<Double> getTranslationY,
                    @JsonProperty("getRotationAngle") List<Double> getRotationAngle, World world) {

        this.time = time;
        this.bounds = bounds;
        this.bodyCount = bodyCount;
        this.bodyListColor = bodyListColor;
        this.jointCount = jointCount;
        this.getReactionForce = getReactionForce;
        this.worms = worms;
        this.temperatureMatrix = temperatureMatrix;
        this.wormsSize = wormSize;
        this.listOfFixtureList = listOfFixtureList;

        this.getTranslationX = getTranslationX;
        this.getTranslationY = getTranslationY;
        this.getRotationAngle = getRotationAngle;

    }

    public Snapshot(double time, World world, List<Worm> worms, WorldTemperature temperature) {
        this.time = time;
        Gson gson = new Gson();

        this.bounds = gson.fromJson(gson.toJson(world.getBounds()), Bounds.class);
        this.bodyCount = world.getBodyCount();
        this.bodyListColor = new ArrayList<>();
        this.getTranslationX = new ArrayList<>();
        this.getTranslationY = new ArrayList<>();
        this.getRotationAngle = new ArrayList<>();

        for (int i = 0; i < world.getBodyCount(); i++) {
            SimulationBody body = (SimulationBody) world.getBody(i);
            this.getTranslationX.add(body.getTransform().getTranslationX());
            this.getTranslationY.add(body.getTransform().getTranslationY());
            this.getRotationAngle.add(body.getTransform().getRotationAngle());
            this.bodyListColor
                    .add(new Color(body.getColor().getRed(), body.getColor().getGreen(), body.getColor().getBlue()));
        }
        this.listOfFixtureList = new ArrayList<>();
        Cloner cloner = new Cloner();
        for (int i = 0; i < world.getBodyCount(); i++) {
            List<BodyFixture> fixList = new ArrayList<>();
            for (int j = 0; j < world.getBody(i).getFixtures().size(); j++) {
                BodyFixture clonedFixture = cloner.deepClone((BodyFixture) world.getBody(i).getFixtures().get(j));
                fixList.add(clonedFixture);
            }
            listOfFixtureList.add(fixList);
        }

        this.jointCount = world.getJointCount();
        for (int i = 0; i < world.getJointCount(); i++) {

        }
        this.getReactionForce = world.getTimeStep().getInverseDeltaTime();

        this.worms = new ArrayList<>();

        this.wormsSize = worms.size();

        this.temperatureMatrix = new double[temperature.getyWorldDimension()][temperature.getXWorldDimension()];
        for (int i = 0; i < temperature.getyWorldDimension(); i++) {
            for (int j = 0; j < temperature.getXWorldDimension(); j++) {
                this.temperatureMatrix[i][j] = temperature.getMatrix()[i][j];
            }
        }
    }

    public List<Double> getTranslationX() {
        return this.getTranslationX;
    }

    public List<Double> getTranslationY() {
        return this.getTranslationY;
    }

    public List<Double> getRotationAngle() {
        return this.getRotationAngle;
    }

    public double getTime() {
        return this.time;
    }

    public Bounds getBounds() {
        return this.bounds;
    }

    public int getBodyCount() {
        return this.bodyCount;
    }

    public List<Color> getBodyList() {
        return this.bodyListColor;
    }

    public List<Worm> getWorms() {
        return this.worms;
    }

    public double[][] getTemperature() {
        return this.temperatureMatrix;
    }

    public List<List<BodyFixture>> getListofFixtures() {
        return this.listOfFixtureList;
    }

}
