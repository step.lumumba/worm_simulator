package it.units.erallab.wormsimulation;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dyn4j.dynamics.joint.DistanceJoint;
import org.dyn4j.dynamics.joint.RevoluteJoint;
import org.dyn4j.geometry.Geometry;
import org.dyn4j.geometry.Mass;
import org.dyn4j.geometry.Rectangle;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.samples.framework.SimulationBody;
import org.dyn4j.samples.framework.SimulationFrame;
import org.dyn4j.world.World;

public class Worm {

    private double totalDistance = 0;
    private double initialX;
    private double initialY;
    private double XMax;
    private double YMax;
    private double differenceX = 0;
    private double differenceY = 0;

    private double recentTotalDistance = 0;
    private double recentInitialX;
    private double recentInitialY;
    private double recentXMax;
    private double recentYMax;
    private double recentDifferenceX = 0;
    private double recentDifferenceY = 0;

    private Map<Long, Double> enMap;
    private Map<Long, double[]> noseMap;
    private Map<Long, double[]> touchMap;
    private Map<Long, double[]> touchMapWorm;
    private Map<Long, double[]> touchMapFood;
    private Map<Long, double[]> touchMapSelf;


    private Map<Long, Double> tempMap;
    private Map<Long, double[]> lidarMap;
    private Map<Long, Double> humanPresenceMap;

    public static final int nOfInputsMlp = 40;
    public static final int nOfOutputsMlp = 3;
    public static final int innerNeurons = 10;

    private int index;

    private final double[] genotype;

    private final long T;

    // # of circles per worm
    private final int numCircles;
    // color of the worm
    private final Color HsbColor;
    // # of mini rectangles that form a flagella
    private final int numFlagelli;
    // height of each mini rectangle
    private final double flagelliLength;

    private int generation;

    private int selectionGeneration;

    private Color tempColor;

    private double currentEnergy;

    private double humanPresence;

    private long age;

    private final long initialMoment;

    private int[] currentFoodItems;

    private double currentClosestItemDistance;

    private double currentTouchRight;

    private double currentTouchLeft;

    private double currentTouchWormRight;
    private double currentTouchWormLeft;

    private double currentTouchFoodRight;
    private double currentTouchFoodLeft;

    private double currentTouchSelfRight;
    private double currentTouchSelfLeft;

    private Vector2 currentHeadPosition;
    private Vector2 pastHeadPosition;

    private Vector2 currentFirstBodyPosition;

    private Lidar lidarSensor;

    private Touch touchSensorRight;
    private Touch touchSensorLeft;

    private Touch touchSensorWormRight;
    private Touch touchSensorWormLeft;

    private Touch touchSensorFoodRight;
    private Touch touchSensorFoodLeft;

    private Touch touchSensorSelfRight;
    private Touch touchSensorSelfLeft;


    private Nose noseSensor;

    private MultiLayerPerceptron mlp;

    private double xCoord;
    private double yCoord;

    private List<SimulationBody> allBodies;
    private List<SimulationBody> allFlagelsRight;
    private List<SimulationBody> allFlagelsLeft;
    private List<SimulationBody> allFlagels;
    private List<SimulationBody> bodies;
    // creation of flagelli of the worm
    private final List<List<SimulationBody>> flagelsRight = new ArrayList<>();
    private List<List<SimulationBody>> flagelsLeft = new ArrayList<>();
    private List<SimulationBody> flagelListRight;
    private List<SimulationBody> flagelListLeft;

    private World world;
    private long startTime;

    final double ballRadius = 1.5;
    final double ballDensity = 3.97925;
    final double ballFriction = 8.08;
    final double ballRestitution = 0.8;
    final int numCirclesMax = 20;
    final int numCirclesMin = 5;
    final int numFlagelsMax = 4;
    final int numFlagelsMin = 2;
    final double flagelsLengthMin = 0.8;
    final double flagelsLengthMax = 1.8;
    final int colorMin = 0;
    final int colorMax = 9;
    final int TMax = 6;
    final int TMin = 2;


    public Worm(double[] genotype, World world, double xCoord, double yCoord, int gen, int gen2) {
        this.genotype = genotype;

        //mapping genotype-phenotype
        double genot0 = Math.round(((genotype[0] + 1) / 2) * (numCirclesMax - numCirclesMin) + numCirclesMin);
        if (genot0 > numCirclesMax) {
            genot0 = numCirclesMax;
        } else if (genot0 < numCirclesMin) {
            genot0 = numCirclesMin;
        }
        double genot1 = Math.round(((genotype[1] + 1) / 2) * (numFlagelsMax - numFlagelsMin) + numFlagelsMin);
        if (genot1 > numFlagelsMax) {
            genot1 = numFlagelsMax;
        } else if (genot1 < numFlagelsMin) {
            genot1 = numFlagelsMin;
        }
        double genot2 = ((genotype[2] + 1) / 2) + flagelsLengthMin;
        if (genot2 > flagelsLengthMax) {
            genot2 = flagelsLengthMax;
        } else if (genot2 < flagelsLengthMin) {
            genot2 = flagelsLengthMin;
        }
        double genot3 = Math.round(((genotype[3] + 1) / 2) * (colorMax - colorMin) + colorMin);
        if (genot3 > colorMax) {
            genot3 = colorMax;
        } else if (genot3 < colorMin) {
            genot3 = colorMin;
        }
        double genot4 = Math.round(((genotype[4] + 1) / 2) * (TMax - TMin) + TMin);
        if (genot4 > TMax) {
            genot4 = TMax;
        } else if (genot4 < TMin) {
            genot4 = TMin;
        }
        this.T = (long) genot4;
        this.selectionGeneration = gen2;
        this.generation = gen;
        this.numCircles = (int) genot0;

        this.HsbColor = Color.getHSBColor((float) (genot3 * 0.1), 1.0f, 0.5f);
        this.tempColor = Color.getHSBColor((float) (genot3 * 0.1), 1.0f, 0.5f);
        this.numFlagelli = (int) genot1;
        this.flagelliLength = genot2;
        this.lidarSensor = new Lidar();
        this.noseSensor = new Nose();
        this.currentEnergy = 100;
        this.humanPresence = 0;
        this.currentFoodItems = new int[]{0, 0};
        this.flagelListRight = new ArrayList<>();
        this.flagelListLeft = new ArrayList<>();
        this.allFlagelsRight = new ArrayList<>();
        this.allFlagelsLeft = new ArrayList<>();
        this.allFlagels = new ArrayList<>();
        this.bodies = new ArrayList<>();
        this.world = world;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.age = 0;
        this.initialMoment = SimulationControl.simulatedTime;
        this.startTime = System.nanoTime();
        this.mlp = new MultiLayerPerceptron(MultiLayerPerceptron.ActivationFunction.TANH, nOfInputsMlp,
                new int[]{innerNeurons}, nOfOutputsMlp);
        double[] paramsMlp = new double[mlp.getParams().length];
        System.arraycopy(genotype, 5, paramsMlp, 0, mlp.getParams().length + 5 - 5);
        this.mlp.setParams(paramsMlp);
        this.enMap = new HashMap<>();
        this.noseMap = new HashMap<>();
        this.touchMap = new HashMap<>();
        this.touchMapWorm = new HashMap<>();
        this.touchMapFood = new HashMap<>();
        this.touchMapSelf = new HashMap<>();

        this.tempMap = new HashMap<>();
        this.lidarMap = new HashMap<>();
        this.humanPresenceMap = new HashMap<>();
        assemble();
    }

    private void assemble() {
        for (int k = 0; k < this.numFlagelli; k++) {
            List<SimulationBody> flagelListRight = new ArrayList<>();
            List<SimulationBody> flagelListLeft = new ArrayList<>();
            for (int i = 0; i < numCircles; i++) {
                flagelListRight.add(new SimulationBody(this.HsbColor));
                flagelListLeft.add(new SimulationBody(this.HsbColor));
            }
            flagelsRight.add(flagelListRight);
            flagelsLeft.add(flagelListLeft);
        }

        for (int i = 0; i < numCircles; i++) {
            if (i == 0) {
                bodies.add(new SimulationBody(Color.getHSBColor(0.1f, 1.0f, 0f)));
            } else {
                bodies.add(new SimulationBody(this.HsbColor));
            }
        }

        double[] transCircles = new double[numCircles];
        transCircles[0] = 1.0;
        for (int k = 1; k < transCircles.length; k++) {
            transCircles[k] = transCircles[k - 1] + 2.0;
        }

        for (SimulationBody body : bodies) {
            body.addFixture(Geometry.createCircle(ballRadius), ballDensity, ballFriction, ballRestitution);
            body.setMass(new Mass(body.getWorldCenter(), 10, 20));
            body.translate(transCircles[bodies.indexOf(body)] + xCoord, yCoord);
            body.setLinearDamping(0.5);
            body.setAngularDamping(0.1);
            if (bodies.indexOf(body) == 0) {
                SimulationFrame.wormsHeads.add(body);
            }
            this.world.addBody(body);
            body.setUserData(this);
        }

        // setting fixtures for the flagelli and adding them to the world
        for (List<SimulationBody> flagListRight : flagelsRight) {
            if (flagelsRight.indexOf(flagListRight) == 0) {
                for (SimulationBody flag : flagListRight) {
                    flag.addFixture(new Rectangle(0.1, this.flagelliLength));
                    flag.setMass(new Mass(flag.getWorldCenter(), 0.2, 2));
                    flag.translate(bodies.get(flagListRight.indexOf(flag)).getWorldCenter().x + ballRadius,
                            bodies.get(flagListRight.indexOf(flag)).getWorldCenter().y + ballRadius);
                    flag.setLinearDamping(0.4);
                    flag.setAngularDamping(0.1);
                    this.world.addBody(flag);

                    flag.setUserData(this);
                }
            } else {
                for (SimulationBody flag : flagListRight) {
                    flag.addFixture(new Rectangle(0.1, this.flagelliLength));
                    flag.setMass(new Mass(flag.getWorldCenter(), 0.2, 2));
                    flag.translate(bodies.get(flagListRight.indexOf(flag)).getWorldCenter().x + ballRadius,
                            flagelsRight.get(flagelsRight.indexOf(flagListRight) - 1).get(flagListRight.indexOf(flag))
                                    .getWorldCenter().y + ballRadius);
                    flag.setLinearDamping(0.4);
                    flag.setAngularDamping(0.1);
                    this.world.addBody(flag);

                    flag.setUserData(this);
                    if (flagelsRight.indexOf(flagListRight) == flagelsRight.size() - 1) {
                        allFlagelsRight.add(flag);
                        allFlagels.add(flag);
                    }
                }
            }
        }

        for (List<SimulationBody> flagListLeft : flagelsLeft) {
            if (flagelsLeft.indexOf(flagListLeft) == 0) {
                for (SimulationBody flag : flagListLeft) {
                    flag.addFixture(new Rectangle(0.1, this.flagelliLength));
                    // flag.setMass(MassType.NORMAL);
                    flag.setMass(new Mass(flag.getWorldCenter(), 0.2, 2));
                    flag.translate(bodies.get(flagListLeft.indexOf(flag)).getWorldCenter().x - ballRadius,
                            bodies.get(flagListLeft.indexOf(flag)).getWorldCenter().y - ballRadius);
                    flag.setLinearDamping(0.4);
                    flag.setAngularDamping(0.1);
                    this.world.addBody(flag);

                    flag.setUserData(this);
                }
            } else {
                for (SimulationBody flag : flagListLeft) {
                    flag.addFixture(new Rectangle(0.1, this.flagelliLength));
                    flag.setMass(new Mass(flag.getWorldCenter(), 0.2, 2));
                    flag.translate(bodies.get(flagListLeft.indexOf(flag)).getWorldCenter().x - ballRadius,
                            flagelsLeft.get(flagelsLeft.indexOf(flagListLeft) - 1).get(flagListLeft.indexOf(flag))
                                    .getWorldCenter().y - ballRadius);
                    flag.setLinearDamping(0.4);
                    flag.setAngularDamping(0.1);
                    this.world.addBody(flag);

                    flag.setUserData(this);
                    if (flagelsLeft.indexOf(flagListLeft) == flagelsLeft.size() - 1) {
                        allFlagelsLeft.add(flag);
                        allFlagels.add(flag);
                    }
                }
            }
        }

        SimulationControl.listOfAllFlagelsList.add(allFlagels);
        // creation of joints for the bodies and flagelli and adding them to the world
        for (SimulationBody body : bodies) {
            if (bodies.indexOf(body) < numCircles - 1) {
                if (bodies.indexOf(body) == 0) {
                    DistanceJoint<SimulationBody> jointHead1 = new DistanceJoint<SimulationBody>(body,
                            bodies.get(bodies.indexOf(body) + 1), body.getWorldCenter().add(new Vector2(0, 1.5)),
                            bodies.get(bodies.indexOf(body) + 1).getWorldCenter());
                    DistanceJoint<SimulationBody> jointHead2 = new DistanceJoint<SimulationBody>(body,
                            bodies.get(bodies.indexOf(body) + 1), body.getWorldCenter().add(new Vector2(0, -1.5)),
                            bodies.get(bodies.indexOf(body) + 1).getWorldCenter());
                    this.world.addJoint(jointHead1);
                    this.world.addJoint(jointHead2);
                    RevoluteJoint<SimulationBody> jointRevoluteHead = new RevoluteJoint<SimulationBody>(body,
                            bodies.get(bodies.indexOf(body) + 1), body.getWorldCenter());
                    this.world.addJoint(jointRevoluteHead);
                } else {
                    DistanceJoint<SimulationBody> joint1 = new DistanceJoint<SimulationBody>(body,
                            bodies.get(bodies.indexOf(body) + 1), body.getWorldCenter(),
                            bodies.get(bodies.indexOf(body) + 1).getWorldCenter());
                    this.world.addJoint(joint1);
                }

            }

            for (List<SimulationBody> flagListRight : flagelsRight) {
                if (flagelsRight.indexOf(flagListRight) == 0) {
                    DistanceJoint<SimulationBody> flagelJoint = new DistanceJoint<SimulationBody>(body,
                            flagListRight.get(bodies.indexOf(body)),
                            body.getWorldCenter().add(new Vector2(0, ballRadius)),
                            flagListRight.get(bodies.indexOf(body)).getWorldCenter().add(
                                    new Vector2(0, -this.flagelliLength / 2 - (this.flagelliLength * (0.8 / 100)))));
                    flagelJoint.setDistance(0.1);
                    this.world.addJoint(flagelJoint);
                } else {
                    DistanceJoint<SimulationBody> flagelJoint = new DistanceJoint<SimulationBody>(
                            flagelsRight.get(flagelsRight.indexOf(flagListRight) - 1).get(bodies.indexOf(body)),
                            flagListRight.get(bodies.indexOf(body)),
                            flagelsRight.get(flagelsRight.indexOf(flagListRight) - 1).get(bodies.indexOf(body))
                                    .getWorldCenter()
                                    .add(new Vector2(0, this.flagelliLength / 2 - (this.flagelliLength * (0.8 / 100)))),
                            flagListRight.get(bodies.indexOf(body)).getWorldCenter().add(
                                    new Vector2(0, -this.flagelliLength / 2 - (this.flagelliLength * (0.8 / 100)))));
                    flagelJoint.setDistance(0.1);
                    this.world.addJoint(flagelJoint);
                }
            }

            for (List<SimulationBody> flagListLeft : flagelsLeft) {
                if (flagelsLeft.indexOf(flagListLeft) == 0) {
                    DistanceJoint<SimulationBody> flagelJoint = new DistanceJoint<SimulationBody>(body,
                            flagListLeft.get(bodies.indexOf(body)),
                            body.getWorldCenter().add(new Vector2(0, -ballRadius)),
                            flagListLeft.get(bodies.indexOf(body)).getWorldCenter().add(
                                    new Vector2(0, this.flagelliLength / 2 - (this.flagelliLength * (0.8 / 100)))));
                    flagelJoint.setDistance(0.1);
                    this.world.addJoint(flagelJoint);
                } else {
                    DistanceJoint<SimulationBody> flagelJoint = new DistanceJoint<SimulationBody>(
                            flagelsLeft.get(flagelsLeft.indexOf(flagListLeft) - 1).get(bodies.indexOf(body)),
                            flagListLeft.get(bodies.indexOf(body)),
                            flagelsLeft.get(flagelsLeft.indexOf(flagListLeft) - 1).get(bodies.indexOf(body))
                                    .getWorldCenter()
                                    .add(new Vector2(0,
                                            -this.flagelliLength / 2 - (this.flagelliLength * (0.8 / 100)))),
                            flagListLeft.get(bodies.indexOf(body)).getWorldCenter().add(
                                    new Vector2(0, this.flagelliLength / 2 - (this.flagelliLength * (0.8 / 100)))));
                    flagelJoint.setDistance(0.1);
                    this.world.addJoint(flagelJoint);
                }
            }
        }

        this.touchSensorRight = new Touch(this.allFlagelsRight);
        this.touchSensorLeft = new Touch(this.allFlagelsLeft);

        this.touchSensorWormRight = new Touch(this.allFlagelsRight);
        this.touchSensorWormLeft = new Touch(this.allFlagelsLeft);

        this.touchSensorFoodRight = new Touch(this.allFlagelsRight);
        this.touchSensorFoodLeft = new Touch(this.allFlagelsLeft);

        this.touchSensorSelfRight = new Touch(this.allFlagelsRight);
        this.touchSensorSelfLeft = new Touch(this.allFlagelsLeft);

    }

    public World getWorld() {
        return this.getWorld();
    }

    public double[] getGenotype() {
        return this.genotype;
    }

    public Color getGenoColor() {
        return this.HsbColor;
    }

    public int getCircles() {
        return this.numCircles;
    }

    public Color getColor() {
        return this.tempColor;
    }

    public int getFlagelli() {
        return this.numFlagelli;
    }

    public double getFlagelliLength() {
        return this.flagelliLength;
    }

    public Vector2 getHeadPosition() {
        return this.currentHeadPosition;
    }

    public void setHeadPosition(Vector2 position) {
        this.currentHeadPosition = position;
    }

    public void setPastHeadPosition(Vector2 position) {
        this.pastHeadPosition = position;
    }

    public Lidar getLidar() {
        return this.lidarSensor;
    }

    public Touch getTouchWormRight() {
        return this.touchSensorWormRight;
    }

    public Touch getTouchWormLeft() {
        return this.touchSensorWormLeft;
    }

    public Touch getTouchFoodRight() {
        return this.touchSensorFoodRight;
    }

    public Touch getTouchFoodLeft() {
        return this.touchSensorFoodLeft;
    }

    public Touch getTouchSelfRight() {
        return this.touchSensorSelfRight;
    }

    public Touch getTouchSelfLeft() {
        return this.touchSensorSelfLeft;
    }

    public Nose getNose() {
        return this.noseSensor;
    }

    public double getCurrentEnergy() {
        return this.currentEnergy;
    }

    public List<SimulationBody> getBodies() {
        return this.bodies;
    }

    public void setEnergy(int newEnergy) {
        this.currentEnergy = newEnergy;
    }

    public void setHumanPresence(double presence) {
        this.humanPresence = presence;
    }

    public double getHumanPresence() {
        return this.humanPresence;
    }

    public void setCurrentFoodItems(int[] food) {
        this.currentFoodItems = food;
    }

    public int[] getCurrentFoodItems() {
        return this.currentFoodItems;
    }

    public void setCurrentClosestItemDistance(double distance) {
        this.currentClosestItemDistance = distance;
    }

    public double getCurrentClosestItemDistance() {
        return this.currentClosestItemDistance;
    }

    public void setCurrentTouchWormRight(double touch) {
        this.currentTouchWormRight = touch;
    }

    public void setCurrentTouchWormLeft(double touch) {
        this.currentTouchWormLeft = touch;
    }

    public void setCurrentTouchFoodRight(double touch) {
        this.currentTouchFoodRight = touch;
    }

    public void setCurrentTouchFoodLeft(double touch) {
        this.currentTouchFoodLeft = touch;
    }

    public void setCurrentTouchSelfRight(double touch) {
        this.currentTouchSelfRight = touch;
    }

    public void setCurrentTouchSelfLeft(double touch) {
        this.currentTouchSelfLeft = touch;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }

    public double getCurrentTouchRight() {
        return this.currentTouchRight;
    }

    public double getCurrentTouchWormRight() {
        return this.currentTouchWormRight;
    }

    public double getCurrentTouchSelfRight() {
        return this.currentTouchSelfRight;
    }

    public double getCurrentTouchFoodRight() {
        return this.currentTouchFoodRight;
    }

    public long getAge() {
        return this.age;
    }

    public long getT() {
        return this.T;
    }

    public void upadteAge() {
        this.age = SimulationControl.simulatedTime - this.initialMoment;
    }

    public double getCurrentTouchLeft() {
        return this.currentTouchLeft;
    }

    public double getCurrentTouchWormLeft() {
        return this.currentTouchWormLeft;
    }

    public double getCurrentTouchFoodLeft() {
        return this.currentTouchFoodLeft;
    }

    public double getCurrentTouchSelfLeft() {
        return this.currentTouchSelfLeft;
    }

    public MultiLayerPerceptron getMultiLayerPerceptron() {
        return this.mlp;
    }

    public int getGeneration() {
        return this.generation;
    }

    public int getSelectionGeneration() {
        return this.selectionGeneration;
    }

    public Map<Long, Double> getEnMap() {
        return this.enMap;
    }

    public Map<Long, double[]> getNoseMap() {
        return this.noseMap;
    }

    public Map<Long, double[]> getTouchMap() {
        return this.touchMap;
    }

    public Map<Long, double[]> getTouchMapWorm() {
        return this.touchMapWorm;
    }

    public Map<Long, double[]> getTouchMapFood() {
        return this.touchMapFood;
    }

    public Map<Long, double[]> getTouchMapSelf() {
        return this.touchMapSelf;
    }

    public Map<Long, Double> getTemperatureMap() {
        return this.tempMap;
    }

    public Map<Long, double[]> getLidarMap() {
        return this.lidarMap;
    }

    public Map<Long, Double> getHumanPresenceMap() {
        return this.humanPresenceMap;
    }

    public void setXMax(double x) {
        this.XMax = x;
    }

    public void setYMax(double y) {
        this.YMax = y;
    }

    public double getXMax() {
        return this.XMax;
    }

    public double getYMax() {
        return this.YMax;
    }

    public void setInitialX(double x) {
        this.initialX = x;
    }

    public void setInitialY(double y) {
        this.initialY = y;
    }

    public double getInitialX() {
        return this.initialX;
    }

    public double getInitialY() {
        return this.initialY;
    }

    public void setDifferenceX(double x) {
        this.differenceX = x;
    }

    public void setDifferenceY(double y) {
        this.differenceY = y;
    }

    public double getDifferenceX() {
        return this.differenceX;
    }

    public double getDifferenceY() {
        return this.differenceY;
    }

    public void setTotalDistance(double d) {
        this.totalDistance = d;
    }

    public double getTotalDistance() {
        return this.totalDistance;
    }

    public void setRecentXMax(double x) {
        this.recentXMax = x;
    }

    public void setRecentYMax(double y) {
        this.recentYMax = y;
    }

    public double getRecentXMax() {
        return this.recentXMax;
    }

    public double getRecentYMax() {
        return this.recentYMax;
    }

    public void setRecentInitialX(double x) {
        this.recentInitialX = x;
    }

    public void setRecentInitialY(double y) {
        this.recentInitialY = y;
    }

    public double getRecentInitialX() {
        return this.recentInitialX;
    }

    public double getRecentInitialY() {
        return this.recentInitialY;
    }

    public void setRecentDifferenceX(double x) {
        this.recentDifferenceX = x;
    }

    public void setRecentDifferenceY(double y) {
        this.recentDifferenceY = y;
    }

    public double getRecentDifferenceX() {
        return this.recentDifferenceX;
    }

    public double getRecentDifferenceY() {
        return this.recentDifferenceY;
    }

    public void setRecentTotalDistance(double d) {
        this.recentTotalDistance = d;
    }

    public double getRecentTotalDistance() {
        return this.recentTotalDistance;
    }

    public void setColor(Color color) {
        for (SimulationBody body : this.bodies) {
            if (this.bodies.indexOf(body) != 0) {
                body.setColor(color);
            }
        }
        this.tempColor = color;
    }

    public void kill() {
        for (SimulationBody body : bodies) {
            if (bodies.indexOf(body) == 0) {
                SimulationFrame.wormsHeads.remove(body);
            }
            this.world.removeBody(body);
        }
        for (List<SimulationBody> listFlag : flagelsRight) {
            for (SimulationBody flagel : listFlag) {
                this.world.removeBody(flagel);
            }
        }
        for (List<SimulationBody> listFlag : flagelsLeft) {
            for (SimulationBody flagel : listFlag) {
                this.world.removeBody(flagel);
            }
        }
        optimize();
    }

    private void optimize() {
        this.currentFirstBodyPosition = null;
        this.allBodies = null;
        this.allFlagels = null;
        this.allFlagelsLeft = null;
        this.allFlagelsRight = null;
        this.bodies = null;
        this.currentFoodItems = null;
        this.flagelListLeft = null;
        this.flagelListRight = null;
        this.flagelsLeft = null;
        this.lidarSensor = null;
        this.noseSensor = null;
        this.tempColor = null;
        this.touchSensorLeft = null;
        this.touchSensorRight = null;
        this.enMap = null;
        this.noseMap = null;
        this.tempMap = null;
        this.lidarMap = null;
    }

}
