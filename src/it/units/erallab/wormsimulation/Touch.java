package it.units.erallab.wormsimulation;

import org.dyn4j.samples.framework.SimulationBody;
import org.dyn4j.world.World;

import java.util.List;

public class Touch {

	private final List<SimulationBody> bodyList;

	public Touch(List<SimulationBody> bodyList) {
		this.bodyList = bodyList;
	}

	public double[] touchWorm(World world) {
		int counter = 0;
		for (SimulationBody body : this.bodyList) {
			counter++;
			List<SimulationBody> inContactBodies = world.getInContactBodies(body, true);
			for (SimulationBody inContactBody : inContactBodies) {
				Object userData = inContactBody.getUserData();
				if (userData != body.getUserData() && userData != null) {
					return new double[] { 1d };
				}
			}
		}
		return new double[] { 0d };
	}

	public double[] touchFood(World world) {
		int counter = 0;
		for (SimulationBody body : this.bodyList) {
			counter++;
			List<SimulationBody> inContactBodies = world.getInContactBodies(body, true);
			for (SimulationBody inContactBody : inContactBodies) {
				Object userData = inContactBody.getUserData();
				if (userData == null) {
					return new double[] { 1d };
				}
			}
		}
		return new double[] { 0d };
	}

	public double[] touchSelf(World world) {
		int counter = 0;
		for (SimulationBody body : this.bodyList) {
			counter++;
			List<SimulationBody> inContactBodies = world.getInContactBodies(body, true);
			for (SimulationBody inContactBody : inContactBodies) {
				Object userData = inContactBody.getUserData();
				if (userData == body.getUserData()) {
					return new double[] { 1d };
				}
			}
		}
		return new double[] { 0d };
	}
}
