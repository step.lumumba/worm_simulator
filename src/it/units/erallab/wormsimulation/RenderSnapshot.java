package it.units.erallab.wormsimulation;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferStrategy;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

import org.dyn4j.collision.AxisAlignedBounds;
import org.dyn4j.collision.Bounds;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.AABB;
import org.dyn4j.geometry.Convex;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.samples.framework.Camera;
import org.dyn4j.samples.framework.Graphics2DRenderer;

import com.google.common.base.Stopwatch;

public class RenderSnapshot extends JFrame {
    private static final long serialVersionUID = 7659608187025022915L;

    public static final double NANO_TO_BASE = 1.0e9;

    private final static int FRAME_RATE = 20;
    protected final double xWorldDimension = 420;
    protected final double yWorldDimension = 240;
    protected final Canvas canvas;
    protected final Camera camera;
    private boolean stopped;
    private long last;
    private final ScheduledExecutorService executor;

    public RenderSnapshot(String name, double scale) {
        super(name);
        this.camera = new Camera();
        this.camera.scale = scale;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.executor = Executors.newScheduledThreadPool(4);

        // add a window listener
        this.addWindowListener(new WindowAdapter() {
            /*
             * (non-Javadoc)
             *
             * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
             */
            @Override
            public void windowClosing(WindowEvent e) {
                // before we stop the JVM stop the simulation
                stop();
                super.windowClosing(e);
            }
        });
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        // create a canvas to paint to
        this.canvas = new Canvas();
        this.canvas.setPreferredSize(size);
        this.canvas.setMinimumSize(size);
        this.canvas.setMaximumSize(size);
        this.canvas.setBackground(Color.getHSBColor(0.7f, 0.2f, 0.8f));


        // add the canvas to the JFrame
        this.add(this.canvas);

        // make the JFrame not resizable
        this.setResizable(true);

        // size everything
        this.pack();

        this.canvas.requestFocus();

    }

    public synchronized void stop() {
        this.stopped = true;
    }

    public void start() {
        setVisible(true);

        // initialize the last update time
        this.last = System.nanoTime();
        // don't allow AWT to paint the canvas since we are
        this.canvas.setIgnoreRepaint(true);
        // enable double buffering (the JFrame has to be
        // visible before this can be done)
        this.canvas.createBufferStrategy(2);
        // run a separate thread to do active rendering
        // because we don't want to do it on the EDT
        // start consumer of composed frames
        Runnable drawer = new Runnable() {
            final Stopwatch stopwatch = Stopwatch.createUnstarted();
            Snapshot snap;

            @Override
            public void run() {
                if (!stopwatch.isRunning()) {
                    stopwatch.start();
                }
                synchronized (SimulationControl.snapshotsQueue) {
                    while (!SimulationControl.snapshotsQueue.isEmpty()) {

                        snap = SimulationControl.snapshotsQueue.poll();
                        try {
                            renderFrame(snap);
                            snap = null;

                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                    }
                }

            }
        };
        executor.scheduleAtFixedRate(drawer, Math.round(1 * 1000d), Math.round(1000d / (double) FRAME_RATE),
                TimeUnit.MILLISECONDS);
    }

    private void renderFrame(Snapshot snap) {
        Graphics2D g = (Graphics2D) this.canvas.getBufferStrategy().getDrawGraphics();

        // by default, set (0, 0) to be the center of the screen with the positive x
        // axis
        // pointing right and the positive y axis pointing up
        this.transform(g);

        // reset the view
        this.clear(g);
        long time = System.nanoTime();
        // get the elapsed time from the last iteration
        long diff = time - this.last;
        // set the last time
        this.last = time;
        // convert from nanoseconds to seconds
        double elapsedTime = (double) diff / NANO_TO_BASE;
        // render anything about the simulation (will render the World objects)
        AffineTransform tx = g.getTransform();
        g.translate(this.camera.offsetX, this.camera.offsetY);
        this.render(g, elapsedTime, snap);
        g.setTransform(tx);
        g.dispose();

        // blit/flip the buffer
        BufferStrategy strategy = this.canvas.getBufferStrategy();
        if (!strategy.contentsLost()) {
            strategy.show();
        }

        // Sync the display on some systems.
        // (on Linux, this fixes event queue problems)
        Toolkit.getDefaultToolkit().sync();
    }

    protected void transform(Graphics2D g) {
        final int w = this.canvas.getWidth();
        final int h = this.canvas.getHeight();

        // before we render everything im going to flip the y axis and move the
        // origin to the center (instead of it being in the top left corner)
        AffineTransform yFlip = AffineTransform.getScaleInstance(1, -1);
        AffineTransform move = AffineTransform.getTranslateInstance(w / 2, -h / 2);
        g.transform(yFlip);
        g.transform(move);
    }

    protected void clear(Graphics2D g) {
        final int w = this.canvas.getWidth();
        final int h = this.canvas.getHeight();

        // lets draw over everything with a white background
        g.setColor(Color.getHSBColor(0.7f, 0.2f, 0.8f));

        //g.setColor(Color.getHSBColor(0.172f, 0.2f, 0.8f));
        g.fillRect(-w / 2, -h / 2, w, h);
    }

    protected void render(Graphics2D g, double elapsedTime, Snapshot snap) {
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // draw the bounds (if set)
        Bounds bounds = snap.getBounds();
        if (bounds instanceof AxisAlignedBounds) {
            AxisAlignedBounds aab = (AxisAlignedBounds) bounds;
            AABB aabb = aab.getBounds();
            Rectangle2D.Double ce = new Rectangle2D.Double(aabb.getMinX() * this.camera.scale,
                    aabb.getMinY() * this.camera.scale, aabb.getWidth() * this.camera.scale,
                    aabb.getHeight() * this.camera.scale);
            g.setColor(new Color(128, 0, 128));
            g.draw(ce);
        }
        int[] YDim = new int[(int) Math.ceil(yWorldDimension / 2)];
        YDim[0] = 1;
        for (int i = 1; i < (int) Math.ceil(yWorldDimension / 2); i++) {
            YDim[i] = YDim[i - 1] + 3;
        }
        int[] XDim = new int[(int) Math.ceil(xWorldDimension / 2)];
        XDim[0] = 1;
        for (int i = 1; i < (int) Math.ceil(xWorldDimension / 2); i++) {
            XDim[i] = XDim[i - 1] + 3;
        }
        for (int i = 0; i < yWorldDimension; i++) {
            for (int j = 0; j < xWorldDimension; j++) {
                if (snap.getTemperature()[i][j] <= 0) {

                }
                if (isContained(i, YDim) && isContained(j, XDim)) {
                    if (snap.getTemperature()[i][j] > 0 && snap.getTemperature()[i][j] < 1) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        g.setColor(new Color(0.89f, 0.15f, 0.21f, (float) snap.getTemperature()[i][j]));
                    }
                    if (snap.getTemperature()[i][j] >= 1 && snap.getTemperature()[i][j] < 4) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        float alpha = (float) ((snap.getTemperature()[i][j] - 1) / 4);
                        g.setColor(new Color(1.0f, 1.0f, 0.10f, (float) ((alpha / 2))));
                        g.fill(temp);
                    }
                    if (snap.getTemperature()[i][j] >= 4 && snap.getTemperature()[i][j] < 10) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        float alpha = (float) ((snap.getTemperature()[i][j] - 4) / 10);
                        g.setColor(new Color(1.0f, 0.75f, 0.0f, (float) (0.3 + (alpha / 2))));
                        g.fill(temp);
                    }
                    if (snap.getTemperature()[i][j] >= 10 && snap.getTemperature()[i][j] < 25) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        float alpha = (float) ((snap.getTemperature()[i][j] - 10) / 25);
                        g.setColor(new Color(1.0f, 0.0f, 0.0f, (float) (0.3 + (alpha / 2))));
                        g.fill(temp);
                    }
                    if (snap.getTemperature()[i][j] >= 25 && snap.getTemperature()[i][j] < 35) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        float alpha = (float) ((snap.getTemperature()[i][j] - 25) / 35);
                        g.setColor(new Color(0.90f, 0.0f, 0.0f, (float) (0.5 + (alpha / 2))));
                        g.fill(temp);
                    }
                    if (snap.getTemperature()[i][j] >= 35 && snap.getTemperature()[i][j] < 60) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        float alpha = (float) ((snap.getTemperature()[i][j] - 35) / 50);
                        g.setColor(new Color(0.40f, 0.0f, 0.0f, (float) (0.5 + (alpha / 2))));
                        g.fill(temp);
                    }
                    if (snap.getTemperature()[i][j] >= 60 && snap.getTemperature()[i][j] < 70) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        g.setColor(new Color(0.35f, 0.0f, 0.0f, 0.7f));
                        g.fill(temp);
                    }
                    if (snap.getTemperature()[i][j] >= 70) {
                        Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                3.0 * this.camera.scale);
                        g.setColor(new Color(0.25f, 0.0f, 0.0f, 0.7f));
                        g.fill(temp);
                    }
                }

            }
        }

        g.setColor(Color.getHSBColor(0.7f, 0.1f, 0.8f));

        for (Worm worm : snap.getWorms()) {
            double direction = worm.getBodies().get(0).getWorldCenter()
                    .subtract(worm.getBodies().get(1).getWorldCenter()).getDirection();
            Vector2 vec = new Vector2(direction);
            Line2D.Double line1 = new Line2D.Double(worm.getBodies().get(0).getWorldCenter().x * this.camera.scale,
                    worm.getBodies().get(0).getWorldCenter().y * this.camera.scale,
                    (worm.getBodies().get(0).getWorldCenter().x + Nose.smellRadius * Math.cos(direction))
                            * this.camera.scale,
                    (worm.getBodies().get(0).getWorldCenter().y + Nose.smellRadius * Math.sin(direction))
                            * this.camera.scale);
            Line2D.Double line2 = new Line2D.Double(worm.getBodies().get(0).getWorldCenter().x * this.camera.scale,
                    worm.getBodies().get(0).getWorldCenter().y * this.camera.scale,
                    (worm.getBodies().get(0).getWorldCenter().x - Nose.smellRadius * Math.cos(direction))
                            * this.camera.scale,
                    (worm.getBodies().get(0).getWorldCenter().y - Nose.smellRadius * Math.sin(direction))
                            * this.camera.scale);
            Ellipse2D.Double e = new Ellipse2D.Double(
                    (worm.getBodies().get(0).getWorldCenter().x - Nose.smellRadius) * this.camera.scale,
                    (worm.getBodies().get(0).getWorldCenter().y - Nose.smellRadius) * this.camera.scale,
                    Nose.smellRadius * 2 * this.camera.scale, Nose.smellRadius * 2 * this.camera.scale);
            g.setColor(new Color(0.26f, 0.38f, 0.47f, 0.4f));
            g.fill(e);
            g.draw(e);
            g.setColor(Color.MAGENTA);
            g.draw(line1);
            g.draw(line2);

        }

        // draw all the objects in the world
        for (int i = 0; i < snap.getBodyCount(); i++) {
            // get the object
            Color color = snap.getBodyList().get(i);
            List<BodyFixture> fixList = snap.getListofFixtures().get(i);
            this.render(g, elapsedTime, color, fixList, snap.getTranslationX().get(i), snap.getTranslationY().get(i), snap.getRotationAngle().get(i));
        }
    }

    private boolean isContained(int number, int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return true;
            }
        }
        return false;
    }

    protected void render(Graphics2D g, double elapsedTime, Color color, List<BodyFixture> fixtureList, double translationX, double translationY, double rotationAngle) {
        // if the object is selected, draw it magenta
        AffineTransform ot = g.getTransform();

        // transform the coordinate system from world coordinates to local coordinates
        AffineTransform lt = new AffineTransform();
        lt.translate(translationX * this.camera.scale, translationY * this.camera.scale);
        lt.rotate(rotationAngle);
        // apply the transform
        g.transform(lt);

        // loop over all the body fixtures for this body
        for (BodyFixture fixture : fixtureList) {
            this.renderFixture(g, this.camera.scale, fixture, color);
        }
        g.setTransform(ot);

    }

    protected void renderFixture(Graphics2D g, double scale, BodyFixture fixture, Color color) {
        // get the shape on the fixture
        Convex convex = fixture.getShape();

        // render the fixture
        Graphics2DRenderer.render(g, convex, scale, color);
    }

}
