package it.units.erallab.wormsimulation;

import java.io.Serializable;

public interface TimedRealFunction extends Serializable {
  double[] apply(double t, double[] input);

  int getInputDimension();

  int getOutputDimension();
}
