package it.units.erallab.wormsimulation;

public interface RealFunction extends TimedRealFunction {
    double[] apply(double[] input);

    @Override
    default double[] apply(double t, double[] input) {
        return apply(input);
    }

}
