package it.units.erallab.wormsimulation;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferStrategy;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;

import javax.swing.JFrame;

import org.dyn4j.collision.AxisAlignedBounds;
import org.dyn4j.collision.Bounds;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.contact.ContactConstraint;
import org.dyn4j.dynamics.contact.SolvedContact;
import org.dyn4j.dynamics.joint.DistanceJoint;
import org.dyn4j.dynamics.joint.Joint;
import org.dyn4j.dynamics.joint.PinJoint;
import org.dyn4j.geometry.AABB;
import org.dyn4j.geometry.Ray;
import org.dyn4j.geometry.Transform;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.samples.framework.Camera;
import org.dyn4j.samples.framework.SimulationBody;
import org.dyn4j.samples.framework.input.MousePanningInputHandler;
import org.dyn4j.samples.framework.input.MousePickingInputHandler;
import org.dyn4j.samples.framework.input.MouseZoomInputHandler;
import org.dyn4j.samples.framework.input.ToggleStateKeyboardInputHandler;
import org.dyn4j.world.World;
import org.dyn4j.world.WorldCollisionData;

public class RenderRealTime extends JFrame {
    public static final double NANO_TO_BASE = 1.0e9;

    public static int canvasWidth;
    public static int canvasHeight;

    protected final Canvas canvas;

    // stop/pause

    /**
     * True if the simulation is exited
     */
    private boolean stopped;

    /**
     * The time stamp for the last iteration
     */
    protected long last;

    // camera
    public static double cameraScale;
    protected final Camera camera;
    protected final World<SimulationBody> world;
    protected final double xWorldDimension = 420;
    protected final double yWorldDimension = 240;

    protected WorldTemperature temperature;

    // interaction (mouse/keyboard)

    private final ToggleStateKeyboardInputHandler paused;
    private final ToggleStateKeyboardInputHandler step;

    private final MousePickingInputHandler picking;
    private final MousePanningInputHandler panning;
    private final MouseZoomInputHandler zoom;

    private final ToggleStateKeyboardInputHandler renderContacts;
    private final ToggleStateKeyboardInputHandler renderBodyAABBs;
    private final ToggleStateKeyboardInputHandler renderBodyRotationRadius;
    private final ToggleStateKeyboardInputHandler renderFixtureAABBs;
    private final ToggleStateKeyboardInputHandler renderFixtureRotationRadius;
    private final ToggleStateKeyboardInputHandler renderLidar;
    private final ToggleStateKeyboardInputHandler renderSmellRadius;
    private final ToggleStateKeyboardInputHandler renderTemperature;
    private final ToggleStateKeyboardInputHandler renderSensorValues;

    private final ToggleStateKeyboardInputHandler faceDetect;


    public RenderRealTime(String name, double scale, World world, WorldTemperature temperature) {
        super(name);
        this.camera = new Camera();
        this.camera.scale = scale;

        // create the world
        this.world = world;
        this.temperature = temperature;

        // this.world.getSettings().setStepFrequency(0.0026);
        // setup the JFrame
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // add a window listener
        this.addWindowListener(new WindowAdapter() {
            /*
             * (non-Javadoc)
             *
             * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
             */
            @Override
            public void windowClosing(WindowEvent e) {
                // before we stop the JVM stop the simulation
                stop();
                super.windowClosing(e);
            }
        });

        // create the size of the window
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();

        // create a canvas to paint to
        this.canvas = new Canvas();
        this.canvas.setPreferredSize(size);
        this.canvas.setMinimumSize(size);
        this.canvas.setMaximumSize(size);
        this.canvas.setBackground(Color.getHSBColor(0.172f, 0.2f, 0.8f));

        // add the canvas to the JFrame
        this.add(this.canvas);

        // make the JFrame not resizable
        this.setResizable(true);

        // size everything
        this.pack();

        this.canvas.requestFocus();

        this.faceDetect = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_P);

        // install input handlers
        this.picking = new MousePickingInputHandler(this.canvas, this.camera, this.world);
        this.picking.install();
        this.panning = new MousePanningInputHandler(this.canvas, this.camera);
        this.panning.install();
        // panning and picking are dependent
        this.picking.getDependentBehaviors().add(this.panning);
        this.panning.getDependentBehaviors().add(this.picking);

        this.zoom = new MouseZoomInputHandler(this.canvas, this.camera, MouseEvent.BUTTON1);
        this.zoom.install();

        this.paused = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_SPACE);
        this.step = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_ENTER);
        this.renderContacts = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_C);
        this.renderBodyAABBs = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_B);
        this.renderBodyRotationRadius = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_B);
        this.renderLidar = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_L);
        this.renderFixtureAABBs = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_F);
        this.renderFixtureRotationRadius = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_F);
        this.renderSmellRadius = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_N);
        this.renderTemperature = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_T);
        this.renderSensorValues = new ToggleStateKeyboardInputHandler(this.canvas, KeyEvent.VK_S);

        this.paused.install();
        this.step.install();
        this.step.setDependentBehaviorsAdditive(true);
        this.step.getDependentBehaviors().add(this.paused);
        this.renderContacts.install();
        this.renderBodyAABBs.install();
        this.renderBodyRotationRadius.install();
        this.renderFixtureAABBs.install();
        this.renderFixtureRotationRadius.install();
        this.renderLidar.install();
        this.renderSmellRadius.install();
        this.renderTemperature.install();
        this.renderSensorValues.install();
        this.faceDetect.install();
    }

    public void start() {
        Graphics2D g = (Graphics2D) this.canvas.getBufferStrategy().getDrawGraphics();
        this.transform(g);
        this.clear(g);
        long time = System.nanoTime();
        // get the elapsed time from the last iteration
        long diff = time - this.last;
        // set the last time
        this.last = time;
        // convert from nanoseconds to seconds
        double elapsedTime = (double) diff / NANO_TO_BASE;

        // render anything about the simulation (will render the World objects)
        AffineTransform tx = g.getTransform();
        g.translate(this.camera.offsetX, this.camera.offsetY);
        this.render(g, elapsedTime);
        g.setTransform(tx);

        g.dispose();

        // blit/flip the buffer
        BufferStrategy strategy = this.canvas.getBufferStrategy();
        if (!strategy.contentsLost()) {
            strategy.show();
        }

        // Sync the display on some systems.
        // (on Linux, this fixes event queue problems)
        Toolkit.getDefaultToolkit().sync();

    }

    protected void transform(Graphics2D g) {
        final int w = this.canvas.getWidth();
        final int h = this.canvas.getHeight();

        // before we render everything im going to flip the y axis and move the
        // origin to the center (instead of it being in the top left corner)
        AffineTransform yFlip = AffineTransform.getScaleInstance(1, -1);
        AffineTransform move = AffineTransform.getTranslateInstance(w / 2, -h / 2);
        g.transform(yFlip);
        g.transform(move);
    }

    protected void clear(Graphics2D g) {
        final int w = this.canvas.getWidth();
        final int h = this.canvas.getHeight();

        // lets draw over everything with a white background
        g.setColor(Color.getHSBColor(0.7f, 0.2f, 0.8f));

        g.fillRect(-w / 2, -h / 2, w, h);
    }

    public synchronized void stop() {
        this.stopped = true;
    }


    protected void render(Graphics2D g, double elapsedTime) {
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // draw the bounds (if set)
        Bounds bounds = this.world.getBounds();
        if (bounds instanceof AxisAlignedBounds) {
            AxisAlignedBounds aab = (AxisAlignedBounds) bounds;
            AABB aabb = aab.getBounds();
            Rectangle2D.Double ce = new Rectangle2D.Double(aabb.getMinX() * this.camera.scale,
                    aabb.getMinY() * this.camera.scale, aabb.getWidth() * this.camera.scale,
                    aabb.getHeight() * this.camera.scale);
            g.setColor(new Color(128, 0, 128));
            g.draw(ce);
        }

        canvasHeight = this.canvas.getBounds().height;
        canvasWidth = this.canvas.getBounds().width;
        cameraScale = this.camera.scale;

        SimulationControl.faceRec = this.faceDetect.isActive();

        int[] YDim = new int[(int) Math.ceil(yWorldDimension / 2)];
        YDim[0] = 1;
        for (int i = 1; i < (int) Math.ceil(yWorldDimension / 2); i++) {
            YDim[i] = YDim[i - 1] + 3;
        }
        int[] XDim = new int[(int) Math.ceil(xWorldDimension / 2)];
        XDim[0] = 1;
        for (int i = 1; i < (int) Math.ceil(xWorldDimension / 2); i++) {
            XDim[i] = XDim[i - 1] + 3;
        }
        if (this.renderTemperature.isActive()) {
            for (int i = 0; i < yWorldDimension; i++) {
                for (int j = 0; j < xWorldDimension; j++) {
                    temperature.getMatrix();
                    if (isContained(i, YDim) && isContained(j, XDim)) {
                        if (temperature.getMatrix()[i][j] > 0 && temperature.getMatrix()[i][j] < 1) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            g.setColor(new Color(0.89f, 0.15f, 0.21f, (float) temperature.getMatrix()[i][j]));
                        }
                        if (temperature.getMatrix()[i][j] >= 1 && temperature.getMatrix()[i][j] < 4) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            float alpha = (float) ((temperature.getMatrix()[i][j] - 1) / 4);
                            g.setColor(new Color(1.0f, 1.0f, 0.10f, (float) ((alpha / 2))));
                            g.fill(temp);
                        }
                        if (temperature.getMatrix()[i][j] >= 4 && temperature.getMatrix()[i][j] < 10) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            float alpha = (float) ((temperature.getMatrix()[i][j] - 4) / 10);
                            g.setColor(new Color(1.0f, 0.75f, 0.0f, (float) (0.3 + (alpha / 2))));
                            g.fill(temp);
                        }
                        if (temperature.getMatrix()[i][j] >= 10 && temperature.getMatrix()[i][j] < 25) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            float alpha = (float) ((temperature.getMatrix()[i][j] - 10) / 25);
                            g.setColor(new Color(1.0f, 0.0f, 0.0f, (float) (0.3 + (alpha / 2))));
                            g.fill(temp);
                        }
                        if (temperature.getMatrix()[i][j] >= 25 && temperature.getMatrix()[i][j] < 35) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            float alpha = (float) ((temperature.getMatrix()[i][j] - 25) / 35);
                            g.setColor(new Color(0.90f, 0.0f, 0.0f, (float) (0.5 + (alpha / 2))));
                            g.fill(temp);
                        }
                        if (temperature.getMatrix()[i][j] >= 35 && temperature.getMatrix()[i][j] < 60) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            float alpha = (float) ((temperature.getMatrix()[i][j] - 35) / 50);
                            g.setColor(new Color(0.40f, 0.0f, 0.0f, (float) (0.5 + (alpha / 2))));
                            g.fill(temp);
                        }
                        if (temperature.getMatrix()[i][j] >= 60 && temperature.getMatrix()[i][j] < 70) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            g.setColor(new Color(0.35f, 0.0f, 0.0f, 0.7f));
                            g.fill(temp);
                        }
                        if (temperature.getMatrix()[i][j] >= 70) {
                            Rectangle2D.Double temp = new Rectangle2D.Double((j - xWorldDimension / 2) * this.camera.scale,
                                    (i - yWorldDimension / 2) * this.camera.scale, 3.0 * this.camera.scale,
                                    3.0 * this.camera.scale);
                            g.setColor(new Color(0.25f, 0.0f, 0.0f, 0.7f));
                            g.fill(temp);
                        }
                    }

                }
            }
        }
        g.setColor(Color.getHSBColor(0.7f, 0.1f, 0.8f));

        if (this.renderLidar.isActive()) {
            for (Ray ray : SimulationControl.rayList) {
                double xEnd = ray.getStart().x + Lidar.rayLength * Math.cos(ray.getDirection());
                double yEnd = ray.getStart().y + Lidar.rayLength * Math.sin(ray.getDirection());
                double xStart = ray.getStart().x;
                double yStart = ray.getStart().y;

                Line2D.Double line = new Line2D.Double(xStart * this.camera.scale, yStart * this.camera.scale,
                        xEnd * this.camera.scale, yEnd * this.camera.scale);
                g.setColor(Color.RED);
                g.draw(line);
            }
            SimulationControl.rayList.clear();
        }

        if (this.renderSmellRadius.isActive()) {
            for (Worm worm : SimulationControl.worms) {
                double direction = worm.getBodies().get(0).getWorldCenter()
                        .subtract(worm.getBodies().get(1).getWorldCenter()).getDirection();
                Vector2 vec = new Vector2(direction);
                Line2D.Double line1 = new Line2D.Double(worm.getBodies().get(0).getWorldCenter().x * this.camera.scale,
                        worm.getBodies().get(0).getWorldCenter().y * this.camera.scale,
                        (worm.getBodies().get(0).getWorldCenter().x + Nose.smellRadius * Math.cos(direction))
                                * this.camera.scale,
                        (worm.getBodies().get(0).getWorldCenter().y + Nose.smellRadius * Math.sin(direction))
                                * this.camera.scale);
                Line2D.Double line2 = new Line2D.Double(worm.getBodies().get(0).getWorldCenter().x * this.camera.scale,
                        worm.getBodies().get(0).getWorldCenter().y * this.camera.scale,
                        (worm.getBodies().get(0).getWorldCenter().x - Nose.smellRadius * Math.cos(direction))
                                * this.camera.scale,
                        (worm.getBodies().get(0).getWorldCenter().y - Nose.smellRadius * Math.sin(direction))
                                * this.camera.scale);
                Ellipse2D.Double e = new Ellipse2D.Double(
                        (worm.getBodies().get(0).getWorldCenter().x - Nose.smellRadius) * this.camera.scale,
                        (worm.getBodies().get(0).getWorldCenter().y - Nose.smellRadius) * this.camera.scale,
                        Nose.smellRadius * 2 * this.camera.scale, Nose.smellRadius * 2 * this.camera.scale);
                g.setColor(new Color(0.26f, 0.38f, 0.47f, 0.4f));
                g.fill(e);
                g.draw(e);
                g.setColor(Color.MAGENTA);
                g.draw(line1);
                g.draw(line2);

            }
        }

        // draw all the objects in the world
        for (int i = 0; i < this.world.getBodyCount(); i++) {
            // get the object
            SimulationBody body = this.world.getBody(i);
            this.render(g, elapsedTime, body);

            // body aabb
            if (this.renderBodyAABBs.isActive()) {
                AABB aabb = this.world.getContinuousCollisionDetectionBroadphaseDetector().getAABB(body);
                Rectangle2D.Double ce = new Rectangle2D.Double(aabb.getMinX() * this.camera.scale,
                        aabb.getMinY() * this.camera.scale, aabb.getWidth() * this.camera.scale,
                        aabb.getHeight() * this.camera.scale);
                g.setColor(Color.CYAN);
                g.draw(ce);
            }

            // body rotation radius
            if (this.renderBodyRotationRadius.isActive()) {
                Vector2 c = body.getWorldCenter();
                double r = body.getRotationDiscRadius();
                Ellipse2D.Double e = new Ellipse2D.Double((c.x - r) * this.camera.scale, (c.y - r) * this.camera.scale,
                        r * 2 * this.camera.scale, r * 2 * this.camera.scale);
                g.setColor(Color.PINK);
                g.draw(e);
            }

            // loop over all the body fixtures for this body
            for (BodyFixture fixture : body.getFixtures()) {
                // fixture AABB
                if (this.renderFixtureAABBs.isActive()) {
                    AABB aabb = this.world.getBroadphaseDetector().getAABB(body, fixture);
                    Rectangle2D.Double ce = new Rectangle2D.Double(aabb.getMinX() * this.camera.scale,
                            aabb.getMinY() * this.camera.scale, aabb.getWidth() * this.camera.scale,
                            aabb.getHeight() * this.camera.scale);
                    g.setColor(Color.MAGENTA);
                    g.draw(ce);
                }

                // fixture radius
                if (this.renderFixtureRotationRadius.isActive()) {
                    Transform tx = body.getTransform();
                    Vector2 c = tx.getTransformed(fixture.getShape().getCenter());
                    double r = fixture.getShape().getRadius();
                    Ellipse2D.Double e = new Ellipse2D.Double((c.x - r) * this.camera.scale,
                            (c.y - r) * this.camera.scale, r * 2 * this.camera.scale, r * 2 * this.camera.scale);
                    g.setColor(Color.CYAN.darker());
                    g.draw(e);
                }
            }
        }

        for (int i = 0; i < this.world.getJointCount(); i++) {
            Joint<SimulationBody> j = this.world.getJoint(i);
            if (j instanceof DistanceJoint) {
                DistanceJoint<SimulationBody> dj = (DistanceJoint<SimulationBody>) j;
                Line2D.Double vn = new Line2D.Double(dj.getAnchor1().x * this.camera.scale,
                        dj.getAnchor1().y * this.camera.scale, dj.getAnchor2().x * this.camera.scale,
                        dj.getAnchor2().y * this.camera.scale);
                double target = dj.getDistance();
                double val = Math.abs(target - dj.getAnchor1().distance(dj.getAnchor2())) * 100;
                int red = (int) Math.floor(Math.min(val, 255));
                g.setColor(new Color(red, 0, 0));
                g.draw(vn);
            } else if (j instanceof PinJoint) {
                PinJoint<SimulationBody> pj = (PinJoint<SimulationBody>) j;
                Line2D.Double vn = new Line2D.Double(pj.getAnchor1().x * this.camera.scale,
                        pj.getAnchor1().y * this.camera.scale, pj.getAnchor2().x * this.camera.scale,
                        pj.getAnchor2().y * this.camera.scale);
                double max = pj.getMaximumForce();
                double val = pj.getReactionForce(this.world.getTimeStep().getInverseDeltaTime()).getMagnitude();
                int red = (int) Math.floor((val / max) * 255);
                g.setColor(new Color(red, 0, 0));

                g.draw(vn);
            }
        }

        if (this.renderContacts.isActive()) {
            this.drawContacts(g);
        }
        int yyScale = 40;
        double height = this.canvas.getHeight();
        double width = this.canvas.getWidth();
        AffineTransform yyFlip = AffineTransform.getScaleInstance(1, -1);
        g.transform(yyFlip);
        g.drawString("Time from start: " + SimulationControl.currentTime, (int) Math.round(-width / 2 + 25),
                (int) Math.round(-height / 2 + yyScale));
        g.transform(yyFlip);

        if (this.faceDetect.isActive()) {
            int yyScale2 = 65;
            double height2 = this.canvas.getHeight();
            double width2 = this.canvas.getWidth();
            AffineTransform yyFlip2 = AffineTransform.getScaleInstance(1, -1);
            g.transform(yyFlip2);
            g.drawString("Face detection: ON", (int) Math.round(-width2 / 2 + 25),
                    (int) Math.round(-height2 / 2 + yyScale2));
            g.transform(yyFlip2);
        } else {
            int yyScale2 = 65;
            double height2 = this.canvas.getHeight();
            double width2 = this.canvas.getWidth();
            AffineTransform yyFlip2 = AffineTransform.getScaleInstance(1, -1);
            g.transform(yyFlip2);
            g.drawString("Face detection: OFF", (int) Math.round(-width2 / 2 + 25),
                    (int) Math.round(-height2 / 2 + yyScale2));
            g.transform(yyFlip2);
        }

        if (this.renderSensorValues.isActive()) {
            int yScale = 40;
            double screenHeight = this.canvas.getHeight();
            double screenWidth = this.canvas.getWidth();
            double boxHeight = 0;
            for (Worm worm : SimulationControl.worms) {
                boxHeight += 30;
            }
            Rectangle2D.Double rect = new Rectangle2D.Double(-screenWidth / 2, -screenHeight / 2, 1240, 20 + boxHeight);
            AffineTransform yFlip = AffineTransform.getScaleInstance(1, -1);
            g.transform(yFlip);
            g.setColor(new Color(1.00f, 1.00f, 1.00f, 0.5f));
            g.fill(rect);
            g.draw(rect);

            for (Worm worm : SimulationControl.worms) {
                try {
                    Rectangle2D.Double rectColor = new Rectangle2D.Double(-screenWidth / 2 + 8,
                            -screenHeight / 2 + yScale - 9, 10, 10);
                    g.setColor(worm.getGenoColor());
                    g.fill(rectColor);
                    g.draw(rectColor);
                    g.setColor(Color.black);

                    g.drawString("Worm " + SimulationControl.worms.indexOf(worm), (int) Math.round(-screenWidth / 2 + 25),
                            (int) Math.round(-screenHeight / 2 + yScale));
                    g.drawString("Current energy: " + worm.getCurrentEnergy(), (int) Math.round(-screenWidth / 2 + 80),
                            (int) Math.round(-screenHeight / 2 + yScale));
                    g.drawString("Food items (left): " + worm.getCurrentFoodItems()[0],
                            (int) Math.round(-screenWidth / 2 + 210), (int) Math.round(-screenHeight / 2 + yScale));
                    g.drawString("Food items (right): " + worm.getCurrentFoodItems()[1],
                            (int) Math.round(-screenWidth / 2 + 320), (int) Math.round(-screenHeight / 2 + yScale));
                    BigDecimal bd = BigDecimal.valueOf(worm.getCurrentClosestItemDistance()).setScale(2,
                            RoundingMode.HALF_UP);

                    double touchRight = worm.getCurrentTouchRight();
                    if (touchRight == 1d) {
                        g.drawString("Touch sensor(right): food", (int) Math.round(-screenWidth / 2 + 657),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    } else if (touchRight == -1d) {
                        g.drawString("Touch sensor(right): other", (int) Math.round(-screenWidth / 2 + 657),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    } else if (touchRight == 2d) {
                        g.drawString("Touch sensor(right): self", (int) Math.round(-screenWidth / 2 + 657),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    } else if (touchRight == 0d) {
                        g.drawString("Touch sensor(right): nothing", (int) Math.round(-screenWidth / 2 + 657),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    }
                    double touchLeft = worm.getCurrentTouchLeft();
                    if (touchLeft == 1d) {
                        g.drawString("Touch sensor(left): food", (int) Math.round(-screenWidth / 2 + 820),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    } else if (touchLeft == -1d) {
                        g.drawString("Touch sensor(left): other", (int) Math.round(-screenWidth / 2 + 820),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    } else if (touchLeft == 2d) {
                        g.drawString("Touch sensor(left): self", (int) Math.round(-screenWidth / 2 + 820),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    } else if (touchLeft == 0d) {
                        g.drawString("Touch sensor(left): nothing", (int) Math.round(-screenWidth / 2 + 820),
                                (int) Math.round(-screenHeight / 2 + yScale));
                    }
                    g.drawString("Gen: " + worm.getGeneration(), (int) Math.round(-screenWidth / 2 + 972),
                            (int) Math.round(-screenHeight / 2 + yScale));
                    g.drawString("Norm distance from closest item: " + bd.doubleValue(),
                            (int) Math.round(-screenWidth / 2 + 440), (int) Math.round(-screenHeight / 2 + yScale));

                    g.drawString(
                            "Avg temperature: "
                                    + temperature.getMeanTemperature(worm.getBodies().get(0).getWorldCenter().x,
                                    worm.getBodies().get(0).getWorldCenter().y),
                            (int) Math.round(-screenWidth / 2 + 1025), (int) Math.round(-screenHeight / 2 + yScale));
                    yScale += 25;
                } catch (NullPointerException e) {
                    System.err.println("Exception " + e);
                }
            }
        }

    }

    private boolean isContained(int number, int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return true;
            }
        }
        return false;
    }

    private void drawContacts(Graphics2D g) {
        Iterator<WorldCollisionData<SimulationBody>> it = this.world.getCollisionDataIterator();
        while (it.hasNext()) {
            WorldCollisionData<SimulationBody> wcd = it.next();

            if (!wcd.isContactConstraintCollision())
                continue;

            ContactConstraint<SimulationBody> cc = wcd.getContactConstraint();
            for (SolvedContact c : cc.getContacts()) {
                // draw the contact point
                final double r = 2.5 / this.camera.scale;
                final double d = r * 2;
                Ellipse2D.Double cp = new Ellipse2D.Double((c.getPoint().x - r) * this.camera.scale,
                        (c.getPoint().y - r) * this.camera.scale, d * this.camera.scale, d * this.camera.scale);
                g.setColor(Color.ORANGE);
                g.fill(cp);
                // check for sensor/enabled
                if (!cc.isSensor() && cc.isEnabled()) {
                    // draw the contact normal
                    Line2D.Double vn = new Line2D.Double(c.getPoint().x * this.camera.scale,
                            c.getPoint().y * this.camera.scale,
                            (c.getPoint().x - cc.getNormal().x * c.getDepth() * 100) * this.camera.scale,
                            (c.getPoint().y - cc.getNormal().y * c.getDepth() * 100) * this.camera.scale);
                    g.setColor(Color.BLUE);
                    g.draw(vn);
                }
            }
        }
    }

    protected void render(Graphics2D g, double elapsedTime, SimulationBody body) {
        // if the object is selected, draw it magenta
        Color color = body.getColor();
        if (this.picking.isEnabled() && this.picking.isActive() && this.picking.getBody() == body) {
            color = Color.MAGENTA;
        }
        // draw the object
        body.render(g, this.camera.scale, color);
    }

    /**
     * Sets mouse panning enabled.
     *
     * @param flag true if mouse panning should be enabled
     */
    public void setMousePanningEnabled(boolean flag) {
        this.panning.setEnabled(flag);
    }

    public void setZoomInputHandler(boolean flag) {
        this.zoom.setEnabled(flag);
    }

}
