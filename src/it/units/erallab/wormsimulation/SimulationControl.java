package it.units.erallab.wormsimulation;

import com.opencsv.CSVWriter;
import it.units.erallab.wormsimulation.start.Starter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Rectangle;
import org.dyn4j.geometry.*;
import org.dyn4j.samples.framework.SimulationBody;
import org.dyn4j.world.World;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;
import java.util.Queue;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Predicate;

;

public class SimulationControl {

    public static SimulationBody objectInteraction = null;
    public static boolean clickForFood = false;
    public static Vector2 pointClicked = null;
    private double cycleStartTime;
    private double lastCycleDuration;

    public static double scaleFrame = 5;

    private final boolean foodAfterDeath;

    private static final ExecutorService executor = Executors.newFixedThreadPool(10);
    private static ExecutorService executorJson = Executors.newFixedThreadPool(10);

    private final long stopCriterion; // criterio di stop della simulazione dopo un certo numero di generazioni
    private int actualGeneration;

    public static boolean faceRec = true;

    private long slowRender = 0;

    public static String currentTime = "";

    public enum FitnessFunction {
        DISTANCEFORMMEDIANAGE, MAXAGE
    }

    public static String filePathGeno = "";

    private long counterHuman = 0;
    private long counterPause = 0;
    private boolean presence = true;

    public static double faceRecX;
    public static double faceRecY;
    public static double faceRecHeight;
    public static double faceRecWidth;

    private static final double[] cycleTimesArray = new double[(1000 - 1) + 1];

    private final String fakeHuman;
    private final String fakeHumanBehaviour;

    public static final double xWorldDimension = 420;
    public static final double yWorldDimension = 240;
    public static final double NANO_TO_BASE = 1.0e9;
    public static Queue<Snapshot> snapshotsQueue;
    public static List<Worm> worms;
    private static long last;
    protected long countIterations;

    public static long simulatedTime;
    public static long simulatedTimeFakeHuman;

    private static long timez;
    public static long startTime;

    private double t = 0d;

    public static CSVWriter writer;
    public static File fileToSave = null;

    public static CSVWriter writer2;
    public static File fileToSave2 = null;

    public static CSVWriter writer3;
    public static File fileToSave3 = null;

    public static CSVWriter writer4;
    public static File fileToSave4 = null;

    public static CSVWriter writer5;
    public static File fileToSave5 = null;

    private final int simulationMode;

    private final FitnessFunction fitnessFunction;

    protected final World<SimulationBody> world;
    public static WorldTemperature temperature;

    private static final List<SimulationBody> foodCoordinates = new ArrayList<>();

    protected static List<Ray> rayList;
    public static List<List<SimulationBody>> listOfAllFlagelsList = new ArrayList<>();
    public static List<List<SimulationBody>> listOfAllBodiesList = new ArrayList<>();

    private final int numOfWorms;
    private final List<File> jsonWorms;

    public static final Random random = new Random();

    private RenderRealTime renderRealTime;
    private RenderSnapshot renderSnap;
    private DaemonThread myThread = null;
    private static final String LIB_BIN = "/it/units/erallab/wormsimulation/";

    VideoCapture webSource = null;
    Mat frame = new Mat();

    CascadeClassifier faceDetector;
    // new CascadeClassifier(
    // SimulationControl.class.getResource("haarcascade_frontalface_alt.xml").getPath().substring(1));
    MatOfRect faceDetections = new MatOfRect();

    class DaemonThread implements Runnable {
        protected volatile boolean runnable = false;

        @Override
        public void run() {
            synchronized (this) {
                while (runnable) {
                    if (faceRec) {
                        if (webSource.grab()) {
                            try {
                                webSource.retrieve(frame);
                                Mat dst = new Mat();
                                Mat dst2 = new Mat();
                                // Core.flip(frame, dst, 1);
                                faceDetector.detectMultiScale(frame, faceDetections);
                                for (Rect rect : faceDetections.toArray()) {
                                    temperature.setMatrixValue((int) Math.round(yWorldDimension - rect.y),
                                            (int) Math.round(xWorldDimension - rect.x), rect.height * rect.width);
                                    String[] toCsv = new String[4];

                                    toCsv[0] = String.valueOf((double) simulatedTime);
                                    toCsv[1] = String.valueOf(
                                            (double) (System.nanoTime() - SimulationControl.startTime) / NANO_TO_BASE);
                                    toCsv[2] = String
                                            .valueOf(xWorldDimension - rect.x - xWorldDimension / 2);
                                    toCsv[3] = String
                                            .valueOf(yWorldDimension - rect.y - yWorldDimension / 2);
                                    runCsvTask(toCsv, SimulationControl.fileToSave4, SimulationControl.writer4);
                                    toCsv = null;
                                    faceRecX = rect.x - xWorldDimension / 2;
                                    faceRecX = rect.y - yWorldDimension / 2;
                                    faceRecHeight = rect.height;
                                    faceRecWidth = rect.width;
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    public SimulationControl(int simulationMode, FitnessFunction fitnessFunction, int numWorms, List<File> jsonFiles,
                             String fakeHuman, String fakeHumanBehaviour, long stop, boolean foodOnDeath) {
        this.numOfWorms = numWorms;
        this.jsonWorms = jsonFiles;
        rayList = new ArrayList<>();
        worms = Collections.synchronizedList(new ArrayList<>());
        last = System.nanoTime();
        this.world = new World<>();
        timez = System.nanoTime();
        startTime = System.nanoTime();
        this.simulationMode = simulationMode;
        this.fitnessFunction = fitnessFunction;
        this.fakeHuman = fakeHuman;
        this.fakeHumanBehaviour = fakeHumanBehaviour;
        this.stopCriterion = stop;
        this.actualGeneration = 0;
        this.foodAfterDeath = foodOnDeath;

        faceDetectorSetup();

        // mode 3 is realTime mode
        if (simulationMode == 3 && fakeHuman.equals("true")) {
            setupRealtimeMode();
        }
    }

    private void setupRealtimeMode() {
        try {
            webSource = new VideoCapture(0); // video capture from default cam myThread =
            myThread = new DaemonThread(); // create object of threat class Thread t = new
            Thread t = new Thread(myThread);
            t.setDaemon(true);
            myThread.runnable = true;
            t.start();
        } catch (Exception e) {
            System.out.println("No camera found");
        }
    }

    private void faceDetectorSetup() {
        try {
            String path = "temp";
            // have to use a stream
            InputStream in = Starter.class.getResourceAsStream(LIB_BIN + "haarcascade_frontalface_alt.xml");
            // always write to different location
            File fileOut = new File(
                    System.getProperty("java.io.tmpdir") + "/" + path + LIB_BIN + "haarcascade_frontalface_alt.xml");
            System.out.println(fileOut);
            OutputStream out = FileUtils.openOutputStream(fileOut);
            IOUtils.copy(in, out);
            in.close();
            out.close();
            this.faceDetector = new CascadeClassifier(fileOut.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        snapshotsQueue = new LinkedList<>();
        this.initializeWorld();
        double t = 0d;
        long steps = 0;
        Thread thread = new Thread(() -> {
            if (simulationMode == 1 || simulationMode == 2) {
                renderSnap = new RenderSnapshot("prova", 4);
                renderSnap.start();
            }
        });

        // mode 1 and 2 are snapshot modes
        if (simulationMode == 1 || simulationMode == 2) {
            thread.setDaemon(true);
            thread.start();
        }

        if (simulationMode == 3) {
            renderRealTime = new RenderRealTime("relTime", scaleFrame, this.world, temperature);
            renderRealTime.setVisible(true);
            renderRealTime.setMousePanningEnabled(true);
            renderRealTime.setZoomInputHandler(true);
            renderRealTime.last = System.nanoTime();
            // don't allow AWT to paint the canvas since we are
            renderRealTime.canvas.setIgnoreRepaint(true);
            // enable double buffering (the JFrame has to be
            // visible before this can be done)
            renderRealTime.canvas.createBufferStrategy(2);
        }

        int cycleCounter = 0;
        // simulation loop
        while (simulatedTime < stopCriterion) {
            cycleStartTime = System.nanoTime();
            outerloop:
            for (Worm worm : worms) {
                // if a user clicks on a worm, evolve the worm
                if (objectInteraction != null) {
                    if (objectInteraction.getUserData() != null) {
                        for (SimulationBody body : worm.getBodies()) {
                            if (objectInteraction.getUserData() == body.getUserData()) {
                                evolve(worm);
                                break outerloop;
                            }
                        }
                    }
                }
            }

            t = t + world.getSettings().getStepFrequency();
            world.step(1);
            steps = steps + 1;

            if (simulationMode == 3 && slowRender % 1 == 0) {
                renderRealTime.start();
            }
            this.handleEvents();
            long time = System.nanoTime();
            double elapsedTime = (double) (time - timez) / NANO_TO_BASE;
            try {
                // remove only older snapshots from the queue
                if (simulationMode == 1) {
                    Snapshot snapshot = new Snapshot(elapsedTime, world, worms, temperature);
                    synchronized (snapshotsQueue) {
                        snapshotsQueue.removeIf(isTooOld(snapshot.getTime()));
                    }
                    snapshotsQueue.offer(snapshot);
                }
                // empty the snapshot's queue
                if (simulationMode == 2) {
                    Snapshot snapshot = new Snapshot(elapsedTime, world, worms, temperature);
                    for (Snapshot snap : snapshotsQueue) {
                        if (snapshot.getTime() - snap.getTime() > 6) {
                            snapshotsQueue.clear();
                        }
                    }
                    snapshotsQueue.offer(snapshot);
                }
            } catch (Exception e) {
                System.err.println("ERROR: " + e);
                e.printStackTrace();
            }
            slowRender++;
            cycleCounter++;
            if (simulatedTime > 1 && simulatedTime < 1000) {
                lastCycleDuration = (System.nanoTime() - cycleStartTime) / NANO_TO_BASE;
                cycleTimesArray[(int) (simulatedTime - 1 - 1)] = lastCycleDuration;
                cycleStartTime = System.nanoTime();
            }
        }
        System.exit(0);

    }

    public static Predicate<Snapshot> isTooOld(double t) {
        return p -> t - p.getTime() > 10;
    }

    private boolean checkWormDistance(List<Double> bodiesXPositions, List<Double> bodiesYPositions, int randX,
                                      int randY, int circles, int distance) {
        for (int i = 0; i < bodiesXPositions.size(); i++) {
            int add = 0;
            for (int j = 0; j < circles; j++) {
                if (Math.abs(Math.round(bodiesXPositions.get(i)) - (randX + add)) <= distance
                        && Math.abs(Math.round(bodiesYPositions.get(i)) - (randY)) <= distance) {
                    return true;
                }
                add += 2;
            }
        }
        return false;
    }

    // method to write a row of a csv
    public static void writeCsvRow(String[] array, File file, CSVWriter writer) {
        try {
            writer = new CSVWriter(new FileWriter(file, true));
            writer.writeNext(array);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // create a new worm
    synchronized private void addWorm(double[] genotype, int gen, int gen2, int index) {

        int randX = random.nextInt(150) - 75;
        int randY = random.nextInt(150) - 75;
        List<Double> bodiesXPositions = new ArrayList<>();
        List<Double> bodiesYPositions = new ArrayList<>();
        if (worms.size() != 0) {
            for (Worm worm : worms) {
                for (SimulationBody body : worm.getBodies()) {
                    bodiesXPositions.add(body.getWorldCenter().x);
                    bodiesYPositions.add(body.getWorldCenter().y);
                }
            }
        }

        while (checkWormDistance(bodiesXPositions, bodiesYPositions, randX, randY,
                (int) Math.round(((Math.tanh(genotype[0]) + 1) / 2) * (20 - 5) + 5), 10)) {
            randX = random.nextInt(150) - 75;
            randY = random.nextInt(150) - 75;
        }
        Worm worm = new Worm(genotype, this.world, randX, randY, gen, gen2);
        worm.setIndex(index);
        worm.setXMax(worm.getBodies().get(0).getWorldCenter().x);
        worm.setYMax(worm.getBodies().get(0).getWorldCenter().y);
        worm.setInitialX(worm.getBodies().get(0).getWorldCenter().x);
        worm.setInitialY(worm.getBodies().get(0).getWorldCenter().y);

        worm.setRecentXMax(worm.getBodies().get(0).getWorldCenter().x);
        worm.setRecentYMax(worm.getBodies().get(0).getWorldCenter().y);
        worm.setRecentInitialX(worm.getBodies().get(0).getWorldCenter().x);
        worm.setRecentInitialY(worm.getBodies().get(0).getWorldCenter().y);
        bodiesXPositions.clear();
        bodiesYPositions.clear();
        synchronized (worms) {
            worms.add(worm);

        }
        temperature.setMatrixValue(randY + (int) yWorldDimension / 2, randX + (int) xWorldDimension / 2, 100);
        temperature.setMatrixValue(randY + 1 + (int) yWorldDimension / 2, randX + (int) xWorldDimension / 2, 100);
        temperature.setMatrixValue(randY - 1 + (int) yWorldDimension / 2, randX + (int) xWorldDimension / 2, 100);
        temperature.setMatrixValue(randY + (int) yWorldDimension / 2, randX + 1 + (int) xWorldDimension / 2, 100);
        temperature.setMatrixValue(randY + (int) yWorldDimension / 2, randX - 1 + (int) xWorldDimension / 2, 100);

    }

    // initialize the world with borders, food and starting worms
    protected void initializeWorld() {
        this.world.setGravity(World.ZERO_GRAVITY);
        temperature = new WorldTemperature((int) xWorldDimension, (int) yWorldDimension);
        // checkWorldTemperature();

        // add floor
        Rectangle floorRect = new Rectangle(xWorldDimension, 1.0);
        SimulationBody floor = new SimulationBody(Color.GRAY);
        floor.addFixture(new BodyFixture(floorRect));
        floor.setMass(MassType.INFINITE);
        floor.translate(0.0, -yWorldDimension / 2);
        this.world.addBody(floor);
        floor.setUserData(floor);

        // add ceiling
        Rectangle ceilingRect = new Rectangle(xWorldDimension, 1.0);
        SimulationBody ceiling = new SimulationBody(Color.GRAY);
        ceiling.addFixture(new BodyFixture(ceilingRect));
        ceiling.setMass(MassType.INFINITE);
        ceiling.translate(0.0, yWorldDimension / 2);
        this.world.addBody(ceiling);
        ceiling.setUserData(ceiling);

        // add right wall
        Rectangle rightWallRect = new Rectangle(1.0, yWorldDimension);
        SimulationBody rightWall = new SimulationBody(Color.GRAY);
        rightWall.addFixture(new BodyFixture(rightWallRect));
        rightWall.setMass(MassType.INFINITE);
        rightWall.translate(xWorldDimension / 2, 0.0);
        this.world.addBody(rightWall);
        rightWall.setUserData(rightWall);

        // add left wall
        Rectangle leftWallRect = new Rectangle(1.0, yWorldDimension);
        SimulationBody leftWall = new SimulationBody(Color.GRAY);
        leftWall.addFixture(new BodyFixture(leftWallRect));
        leftWall.setMass(MassType.INFINITE);
        leftWall.translate(-xWorldDimension / 2, 0.0);
        this.world.addBody(leftWall);
        leftWall.setUserData(leftWall);

        // create worms
        worms = new ArrayList<>();

        if (jsonWorms.size() >= 1) {
            try (BufferedReader br = Files.newBufferedReader(Paths.get(jsonWorms.get(0).toString()))) {

                // CSV file delimiter
                String DELIMITER = ";";

                List<String> genotypesList = new ArrayList<>();
                // read the file line by line
                String line;
                while ((line = br.readLine()) != null) {

                    // convert line into columns
                    String[] columns = line.split(DELIMITER);

                    genotypesList.add(columns[16]);
                    // print all columns
                }
                String[] genotypesString = new String[numOfWorms];
                for (int i = 0; i < numOfWorms; i++) {
                    genotypesString[i] = genotypesList.get(genotypesList.size() - 1 - i);
                    System.out.println(genotypesString[i]);
                }
                for (int i = 0; i < numOfWorms; i++) {
                    String[] array = genotypesString[i].split("/");
                    double[] genotype = new double[array.length];
                    for (int j = 0; j < array.length; j++) {
                        array[j] = array[j].replace("\"", "");
                        genotype[j] = Double.parseDouble(array[j]);
                    }
                    addWorm(genotype, 0, 0, i + 1);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            for (int i = 0; i < numOfWorms; i++) {
                addWorm(randomGeno(), 0, 0, i + 1);
            }
        }

        // items of food
        for (int i = 0; i < 35; i++) {
            setFood();
        }
        timez = System.nanoTime();
    }

    // returns a random genotype
    private double[] randomGeno() {
        int parLength = new MultiLayerPerceptron(MultiLayerPerceptron.ActivationFunction.TANH, Worm.nOfInputsMlp,
                new int[]{Worm.innerNeurons}, Worm.nOfOutputsMlp).getParams().length;

        double[] randomGenotype = new double[5 + parLength];

        for (int i = 0; i < randomGenotype.length; i++) {
            randomGenotype[i] = random.nextDouble() * 2d - 1d;
        }
        return randomGenotype;
    }

    // set food random
    private void setFood() {
        Circle foodRect = new Circle(1.6);
        SimulationBody food = new SimulationBody(Color.GREEN);
        food.addFixture(new BodyFixture(foodRect));
        food.setMass(MassType.NORMAL);
        food.setEnabled(true);

        int xRand = random.nextInt(418) - 209;
        int yRand = random.nextInt(238) - 119;
        List<Double> bodiesXPositions = new ArrayList<>();
        List<Double> bodiesYPositions = new ArrayList<>();
        if (worms.size() != 0) {
            for (Worm worm : worms) {
                for (SimulationBody body : worm.getBodies()) {
                    bodiesXPositions.add(body.getWorldCenter().x);
                    bodiesYPositions.add(body.getWorldCenter().y);
                }
            }
        }

        while (checkWormDistance(bodiesXPositions, bodiesYPositions, xRand, yRand, 1, 20)) {
            xRand = random.nextInt(418) - 209;
            yRand = random.nextInt(238) - 119;
        }
        foodCoordinates.add(food);
        food.translate(xRand, yRand);
        food.setLinearDamping(1.9);
        food.setAngularDamping(0.9);
        this.world.addBody(food);

    }

    // set food where a worm dies
    private void setFoodAfterDeath(double xCoord, double yCoord) {
        Circle foodRect = new Circle(1.6);
        SimulationBody food = new SimulationBody(Color.GREEN);
        food.addFixture(new BodyFixture(foodRect));
        food.setMass(MassType.NORMAL);
        food.setEnabled(true);
        foodCoordinates.add(food);
        food.translate(xCoord, yCoord);
        food.setLinearDamping(1.9);
        food.setAngularDamping(0.9);
        this.world.addBody(food);

    }

    public World getWorld() {
        return world;
    }

    // checks if a worm gets in contact with a food item
    private SimulationBody checkForFood(SimulationBody body) {
        List<SimulationBody> inContactBodies = this.world.getInContactBodies(body, false);
        for (SimulationBody inContactBody : inContactBodies) {
            Object userData = inContactBody.getUserData();
            if (userData == null) {
                return inContactBody;
            }
        }
        return body;
    }

    private static double smallestNumber(double[] array) {
        double smallest = Double.MAX_VALUE;
        int index = 0;
        while (index < array.length) {
            // check if smallest is greater than element
            if (smallest > array[index]) {
                // update smallest
                smallest = array[index];
            }
            index++;
        }
        array = null;
        return smallest;
    }

    private double median(double[] array) {
        Arrays.sort(array);
        double median;
        if (array.length % 2 == 0) {
            median = (array[array.length / 2] + array[array.length / 2 - 1]) / 2;
        } else {
            median = array[array.length / 2];
        }
        return median;
    }

    private Map<Integer, Double> sortMapByValue(Map<Integer, Double> map) {
        LinkedHashMap<Integer, Double> sortedMap = new LinkedHashMap<>();
        ArrayList<Double> list = new ArrayList<>();
        for (Map.Entry<Integer, Double> entry : map.entrySet()) {
            list.add(entry.getValue());
        }
        Collections.sort(list);
        for (Double num : list) {
            for (Entry<Integer, Double> entry : map.entrySet()) {
                if (entry.getValue().equals(num)) {
                    sortedMap.put(entry.getKey(), num);
                }
            }
        }
        return sortedMap;
    }

    // select worm for reproduction
    private Worm selectWorm() {
        double[] age = new double[worms.size()];
        for (int i = 0; i < worms.size(); i++) {
            age[i] = worms.get(i).getAge();
        }
        if (this.fitnessFunction == FitnessFunction.DISTANCEFORMMEDIANAGE) {
            double median = median(age);
            for (Worm wormz : worms) {
                if (wormz.getAge() == median) {
                    worms.indexOf(wormz);
                    break;
                }
            }
            Map<Integer, Double> notSortedMap = new HashMap<>();
            for (int i = 0; i < worms.size(); i++) {
                age[i] = worms.get(i).getAge();
            }
            for (int i = 0; i < worms.size(); i++) {
                notSortedMap.put(i, Math.abs(median - age[i]));
            }
            Map<Integer, Double> sortedMap = sortMapByValue(notSortedMap);
            List<Double> notNormList = new ArrayList<>(sortedMap.values());
            List<Double> normList = new ArrayList<>();
            double sum = 0.0;
            for (Double val : notNormList) sum += val;
            for (Double val : notNormList) normList.add((val / sum));
            Collections.reverse(normList);

            double p = Math.random();
            double cumulativeProbability = 0.0;
            List<Integer> keyList = new ArrayList<>(sortedMap.keySet());
            for (int i = 0; i < normList.size(); i++) {
                int index = keyList.get(i);
                cumulativeProbability += normList.get(i);
                if (p <= cumulativeProbability) {
                    return worms.get(index);
                }
            }
        } else if (this.fitnessFunction == FitnessFunction.MAXAGE) {
            double firstProb = 0.9;
            double[] probDist = new double[age.length];
            if (probDist.length == 1) {
                probDist[0] = 1;
            } else {
                probDist[0] = firstProb;
                for (int i = 1; i < probDist.length; i++) {
                    if (i == 1) {
                        if (probDist.length == 2) {
                            probDist[i] = 1 - firstProb;
                        } else {
                            probDist[i] = (1 - firstProb) / 2;
                        }
                    } else {
                        probDist[i] = ((1 - firstProb) / 2) / (probDist.length - 2);
                    }
                }
            }
            double[] ageTemp = new double[age.length];
            System.arraycopy(age, 0, ageTemp, 0, ageTemp.length);
            double temp;
            for (int i = 0; i < ageTemp.length; i++) {
                for (int j = i + 1; j < ageTemp.length; j++) {
                    if (ageTemp[i] < ageTemp[j]) {
                        temp = ageTemp[i];
                        ageTemp[i] = ageTemp[j];
                        ageTemp[j] = temp;
                    }
                }
            }

            double p = Math.random();
            double cumulativeProbability = 0.0;
            for (int i = 0; i < probDist.length; i++) {
                cumulativeProbability += probDist[i];
                if (p <= cumulativeProbability) {
                    for (int j = 0; j < age.length; j++) {
                        if (ageTemp[i] == age[j]) {
                            return worms.get(j);
                        }
                    }
                }
            }


        }

        return null;
    }

    public boolean randomGenotypeProbability(double rate) {
        double p = Math.random();
        return p <= rate;
    }

    // evolve a selected worm
    private void evolve(Worm worm) {
        double[] genotypeChild;
        int gen2;
        if (randomGenotypeProbability(0.1)) {
            genotypeChild = randomGeno();
            gen2 = 0;
        } else {
            Worm selectedWorm = selectWorm();
            gen2 = selectedWorm.getSelectionGeneration();
            double[] genotypeParent = selectedWorm.getGenotype();
            genotypeChild = new double[genotypeParent.length];

            for (int i = 0; i < genotypeParent.length; i++) {
                genotypeChild[i] = genotypeParent[i] + (random.nextGaussian() * 0.35);
            }
        }

        Vector2 headPosition = worm.getHeadPosition();
        int gen = worm.getGeneration();

        if (this.foodAfterDeath) {
            for (int i = 0; i < worm.getCircles(); i++) {
                double randX = Math.random() * (4.5 + 4.5 + 1) - 4.5;
                double randY = Math.random() * (4.5 + 4.5 + 1) - 4.5;
                setFoodAfterDeath(headPosition.x + randX, headPosition.y + randY);
                int randomFoodItem = random.nextInt(foodCoordinates.size() - 1 - 1) + 1;
                this.world.removeBody(foodCoordinates.get(randomFoodItem));
                foodCoordinates.remove(randomFoodItem);
            }
        }

        int index = worm.getIndex();
        killWorm(worm);
        if ((gen + 1) > this.actualGeneration) {
            this.actualGeneration = gen + 1;
        }
        addWorm(genotypeChild, gen + 1, gen2 + 1, index);
    }

    synchronized private void killWorm(Worm worm) {
        synchronized (worms) {
            worm.kill();
            worms.remove(worm);
        }
    }

    // set the world temperature
    private void setTemperature() {
        double[][] tempMatr = temperature.getMatrix();
        for (int i = 1; i < yWorldDimension - 1; i++) {
            for (int j = 1; j < xWorldDimension - 1; j++) {
                temperature.setMatrixValue(i, j,
                        ((tempMatr[i][j] + tempMatr[i][j + 1] + tempMatr[i][j - 1] + tempMatr[i - 1][j - 1]
                                + tempMatr[i - 1][j] + tempMatr[i - 1][j + 1] + tempMatr[i + 1][j - 1]
                                + tempMatr[i + 1][j] + tempMatr[i + 1][j + 1]) / 9) * 0.99);
            }
        }
    }

    private double softmax(double input, double[] neuronValues) {
        double total = Arrays.stream(neuronValues).map(Math::exp).sum();
        return Math.exp(input) / total;
    }

    public static synchronized void runCsvTask(String[] array, File file, CSVWriter writer) {
        Runnable runnableTask = () -> {
            writeCsvRow(array, file, writer);
        };
        executor.submit(runnableTask);
    }

    protected void handleEvents() {
        double sum = 0;
        for (int i = 0; i < temperature.getXWorldDimension(); i++) {
            for (int j = 0; j < temperature.getyWorldDimension(); j++) {
                sum = sum + temperature.getMatrix()[j][i];
            }
        }
        double avgTemp = sum / (temperature.getXWorldDimension() * temperature.getyWorldDimension());
        countIterations++;
        setTemperature();
        simulatedTime++;
        simulatedTimeFakeHuman++;

        // every tot cycles write a row to csv and log info
        if (simulatedTime % 1000 == 0) {
            writeDataToCsv();
            writeLogInfo();
        }

        fakeHumanOptions();

        // loop on all worms
        outerloop:
        for (Worm worm : worms) {
            try {
                setWormParameters(avgTemp, worm);

                // if a user clicks the screen and the point is not a worm, this adds a food
                // item
                if (clickForFood) {
                    onClickForFoodAction();
                }
                // print elapsed time
                updateCurrentTime();
                // set temperature influenced by worm bodies
                setTemperature(worm);
                // check worm energy and in case of energy <= 0 evolve the worm
                if (worm.getCurrentEnergy() <= 0) {
                    evolve(worm);
                    break;
                }
                // set touch sensors of worm
                setTouchSensors(worm);
                worm.setHeadPosition(worm.getBodies().get(0).getWorldCenter());

                // set vision of a worm with lidar sensor
                double[] res = new double[worm.getLidar().getRayDirections().length];
                res = worm.getLidar().view(worm.getBodies().get(0), this.world, rayList,
                        worm.getBodies().get(1).getWorldCenter());
                List<Double> resList = new ArrayList<>();
                for (double re : res) {
                    resList.add(re);
                }
                if (simulationMode == 0) {
                    rayList.clear();
                }
                worm.setCurrentClosestItemDistance(smallestNumber(res));

                // check if a food item is eaten; if yes increment energy and add another food
                // item
                SimulationBody contactBody = checkForFood(worm.getBodies().get(0));
                if (contactBody != worm.getBodies().get(0)) {
                    if (worm.getCurrentEnergy() <= 80) {
                        worm.setEnergy((int) worm.getCurrentEnergy() + 20);
                    } else {
                        worm.setEnergy(100);
                    }
                    this.world.removeBody(contactBody);
                    foodCoordinates.remove(contactBody);
                    setFood();
                }
                // set the current number of food items smelled
                worm.setCurrentFoodItems(worm.getNose().smell(worm.getBodies().get(0), foodCoordinates,
                        worm.getBodies().get(1).getWorldCenter()));

                // set the current head position
                if (worms.indexOf(worm) == 0) {
                    worm.setHeadPosition(worm.getBodies().get(0).getWorldCenter());
                }
                // update worm age
                worm.upadteAge();

                // get current worm energy
                worm.getEnMap().put(simulatedTime, worm.getCurrentEnergy());
                worm.getNoseMap().put(simulatedTime,
                        new double[]{worm.getCurrentFoodItems()[0], worm.getCurrentFoodItems()[1]});
                worm.getTouchMapWorm().put(simulatedTime,
                        new double[]{worm.getCurrentTouchWormLeft(), worm.getCurrentTouchWormRight()});
                worm.getTouchMapFood().put(simulatedTime,
                        new double[]{worm.getCurrentTouchFoodLeft(), worm.getCurrentTouchFoodRight()});
                worm.getTouchMapSelf().put(simulatedTime,
                        new double[]{worm.getCurrentTouchSelfLeft(), worm.getCurrentTouchSelfRight()});
                worm.getTemperatureMap().put(simulatedTime, temperature.getMeanTemperature(
                        worm.getBodies().get(0).getWorldCenter().x, worm.getBodies().get(0).getWorldCenter().y));
                worm.getLidarMap().put(simulatedTime, res);
                worm.getHumanPresenceMap().put(simulatedTime, worm.getHumanPresence());

                double[] inputs = new double[Worm.nOfInputsMlp];
                System.arraycopy(res, 0, inputs, 0, res.length);
                inputs[res.length] = worm.getCurrentEnergy();
                inputs[res.length + 1] = worm.getCurrentFoodItems()[0];
                inputs[res.length + 2] = worm.getCurrentFoodItems()[1];
                inputs[res.length + 3] = worm.getCurrentTouchWormLeft();
                inputs[res.length + 4] = worm.getCurrentTouchWormRight();
                inputs[res.length + 5] = worm.getCurrentTouchFoodLeft();
                inputs[res.length + 6] = worm.getCurrentTouchFoodRight();
                inputs[res.length + 7] = worm.getCurrentTouchSelfLeft();
                inputs[res.length + 8] = worm.getCurrentTouchSelfRight();
                inputs[res.length + 9] = temperature.getMeanTemperature(worm.getBodies().get(0).getWorldCenter().x,
                        worm.getBodies().get(0).getWorldCenter().y);
                inputs[res.length + 10] = worm.getHumanPresence();

                // get past sensor values
                double[] pastRes;
                pastRes = getPastLidarValues(worm, res);
                double[] pastNose;
                pastNose = getPastNoseValues(worm);
                double[] pastTouch;
                try {
                    pastTouch = worm.getTouchMap().get(simulatedTime - worm.getT());
                    if (pastTouch == null) {
                        throw new NullPointerException();
                    } else {
                        Set<Map.Entry<Long, double[]>> entrySet = worm.getTouchMap().entrySet();
                        List<Long> remove = new ArrayList<>();
                        for (Map.Entry<Long, double[]> entry : entrySet) {
                            if (entry.getKey() < (simulatedTime - worm.getT())) {
                                remove.add(entry.getKey());
                            }
                        }
                        for (Long l : remove) {
                            worm.getTouchMap().remove(l);
                        }
                    }
                } catch (NullPointerException e) {
                    pastTouch = new double[2];
                    pastTouch[0] = 0;
                    pastTouch[1] = 0;
                }
                double[] pastTouchWorm;
                pastTouchWorm = getPastTouchWorm(worm);
                double[] pastTouchFood;
                pastTouchFood = getPastTouchFood(worm);
                double[] pastTouchSelf;
                pastTouchSelf = getPastTouchSelf(worm);
                double pastEnergy;
                pastEnergy = getPastEnergy(worm);
                double pastTemperature;
                pastTemperature = getPastTemperature(worm);
                double pastHumanPresence;
                pastHumanPresence = getPastHumanPresence(worm);

                for (int i = res.length + 11; i < res.length + 11 + res.length; i++) {
                    inputs[i] = (res[i - (res.length + 11)] - pastRes[i - (res.length + 11)]) / worm.getT();
                }
                inputs[2 * res.length + 11] = (worm.getCurrentEnergy() - pastEnergy) / worm.getT();
                inputs[2 * res.length + 12] = (worm.getCurrentFoodItems()[0] - pastNose[0]) / worm.getT();
                inputs[2 * res.length + 13] = (worm.getCurrentFoodItems()[1] - pastNose[1]) / worm.getT();
                inputs[2 * res.length + 14] = (worm.getCurrentTouchWormLeft() - pastTouchWorm[0]) / worm.getT();
                inputs[2 * res.length + 15] = (worm.getCurrentTouchWormRight() - pastTouchWorm[1]) / worm.getT();
                inputs[2 * res.length + 16] = (worm.getCurrentTouchFoodLeft() - pastTouchFood[0]) / worm.getT();
                inputs[2 * res.length + 17] = (worm.getCurrentTouchFoodRight() - pastTouchFood[1]) / worm.getT();
                inputs[2 * res.length + 18] = (worm.getCurrentTouchSelfLeft() - pastTouchSelf[0]) / worm.getT();
                inputs[2 * res.length + 19] = (worm.getCurrentTouchSelfRight() - pastTouchSelf[1]) / worm.getT();
                inputs[2 * res.length + 20] = (inputs[res.length + 9] - pastTemperature) / worm.getT();
                inputs[2 * res.length + 21] = (worm.getHumanPresence() - pastHumanPresence) / worm.getT();

                // outputs of the multilayer perceptron
                double[] outputs = worm.getMultiLayerPerceptron() != null ? worm.getMultiLayerPerceptron().apply(inputs)
                        : new double[Worm.nOfInputsMlp];

                // move worm according to multilayer perceptron outputs
                double normLeft = softmax(outputs[0], outputs);
                double normCenter = softmax(outputs[1], outputs);
                double normRight = softmax(outputs[2], outputs);
                double direction;
                if (normLeft > normCenter && normLeft > normRight) {
                    direction = Math.PI / 2 + worm.getBodies().get(0).getWorldCenter()
                            .subtract(worm.getBodies().get(1).getWorldCenter()).getDirection();
                    Vector2 moveVector = new Vector2(direction);
                    double mag = worm.getBodies().get(0).applyForce(moveVector.multiply(700 * normLeft))
                            .getChangeInPosition().getMagnitude();
                    double totDistance = worm.getTotalDistance();
                    worm.setTotalDistance(totDistance + mag);
                    double recentTotDistance = worm.getRecentTotalDistance();
                    worm.setRecentTotalDistance(recentTotDistance + mag);
                    moveVector = null;
                }
                if (normCenter > normLeft && normCenter > normRight) {
                    direction = Math.PI * 2d + worm.getBodies().get(0).getWorldCenter()
                            .subtract(worm.getBodies().get(1).getWorldCenter()).getDirection();
                    Vector2 moveVector = new Vector2(direction);
                    double mag = worm.getBodies().get(0).applyForce(moveVector.multiply(700 * normCenter))
                            .getChangeInPosition().getMagnitude();
                    double totDistance = worm.getTotalDistance();
                    worm.setTotalDistance(totDistance + mag);
                    double recentTotDistance = worm.getRecentTotalDistance();
                    worm.setRecentTotalDistance(recentTotDistance + mag);
                    moveVector = null;

                }
                if (normRight > normCenter && normRight > normLeft) {
                    direction = -Math.PI / 2 + worm.getBodies().get(0).getWorldCenter()
                            .subtract(worm.getBodies().get(1).getWorldCenter()).getDirection();
                    Vector2 moveVector = new Vector2(direction);
                    double mag = worm.getBodies().get(0).applyForce(moveVector.multiply(700 * normRight))
                            .getChangeInPosition().getMagnitude();
                    double totDistance = worm.getTotalDistance();
                    worm.setTotalDistance(totDistance + mag);
                    double recentTotDistance = worm.getRecentTotalDistance();
                    worm.setRecentTotalDistance(recentTotDistance + mag);
                    moveVector = null;

                }
                inputs = null;
                res = null;
                pastNose = null;
                pastRes = null;
                pastTouch = null;

            } catch (Exception e) {
                System.err.printf("IO error: %s", e);
                e.printStackTrace();
            }
        }

        long time = System.nanoTime();
        long diff = time - last;
        float[] hsbvals = new float[3];

        // every 100 iterations decrease worm energy and color
        if (countIterations == 100) {
            last = time;
            List<List<SimulationBody>> wormBodies = new ArrayList<>();
            for (Worm worm : worms) {
                int energyLoss = 3;
                double colorCoefficient = 0.01;
                wormBodies.add(worms.get(worms.indexOf(worm)).getBodies());
                int newEnergy;
                if ((worm.getCurrentEnergy() - energyLoss) <= 0) {
                    newEnergy = 0;
                } else {
                    newEnergy = (int) worm.getCurrentEnergy() - energyLoss;
                }
                worm.setEnergy(newEnergy);
                hsbvals = Color.RGBtoHSB(worm.getColor().getRed(), worm.getColor().getGreen(),
                        worm.getColor().getBlue(), hsbvals);
                worm.setColor(Color.getHSBColor(hsbvals[0], (float) (worm.getCurrentEnergy() * colorCoefficient),
                        hsbvals[2]));
            }
            countIterations = 0;
        }

    }

    private double getPastHumanPresence(Worm worm) {
        double pastHumanPresence;
        try {
            pastHumanPresence = worm.getHumanPresenceMap().get(simulatedTime - worm.getT());
            Set<Entry<Long, Double>> entrySet = worm.getHumanPresenceMap().entrySet();
            List<Long> remove = new ArrayList<>();
            for (Entry<Long, Double> entry : entrySet) {
                if (entry.getKey() < (simulatedTime - worm.getT())) {
                    remove.add(entry.getKey());
                }
            }
            for (Long l : remove) {
                worm.getHumanPresenceMap().remove(l);
            }
        } catch (NullPointerException e) {
            pastHumanPresence = 0;
        }
        return pastHumanPresence;
    }

    private double getPastTemperature(Worm worm) {
        double pastTemperature;
        try {
            pastTemperature = worm.getTemperatureMap().get(simulatedTime - worm.getT());
            Set<Entry<Long, Double>> entrySet = worm.getTemperatureMap().entrySet();
            List<Long> remove = new ArrayList<>();
            for (Entry<Long, Double> entry : entrySet) {
                if (entry.getKey() < (simulatedTime - worm.getT())) {
                    remove.add(entry.getKey());
                }
            }
            for (Long l : remove) {
                worm.getTemperatureMap().remove(l);
            }
        } catch (NullPointerException e) {
            pastTemperature = 0;
        }
        return pastTemperature;
    }

    private double getPastEnergy(Worm worm) {
        double pastEnergy;
        try {
            pastEnergy = worm.getEnMap().get(simulatedTime - worm.getT());
            if ((Double) pastEnergy == null) {
                throw new NullPointerException();
            } else {
                Set<Entry<Long, Double>> entrySet = worm.getEnMap().entrySet();
                List<Long> remove = new ArrayList<>();
                for (Entry<Long, Double> entry : entrySet) {
                    if (entry.getKey() < (simulatedTime - worm.getT())) {
                        remove.add(entry.getKey());
                    }
                }
                for (Long l : remove) {
                    worm.getEnMap().remove(l);
                }
            }
        } catch (NullPointerException e) {
            pastEnergy = 0;
        }
        return pastEnergy;
    }

    private double[] getPastTouchSelf(Worm worm) {
        double[] pastTouchSelf;
        try {
            pastTouchSelf = worm.getTouchMapSelf().get(simulatedTime - worm.getT());
            if (pastTouchSelf == null) {
                throw new NullPointerException();
            } else {
                Set<Entry<Long, double[]>> entrySet = worm.getTouchMapSelf().entrySet();
                List<Long> remove = new ArrayList<>();
                for (Entry<Long, double[]> entry : entrySet) {
                    if (entry.getKey() < (simulatedTime - worm.getT())) {
                        remove.add(entry.getKey());
                    }
                }
                for (Long l : remove) {
                    worm.getTouchMapSelf().remove(l);
                }
            }
        } catch (NullPointerException e) {
            pastTouchSelf = new double[2];
            pastTouchSelf[0] = 0;
            pastTouchSelf[1] = 0;
        }
        return pastTouchSelf;
    }

    private double[] getPastTouchFood(Worm worm) {
        double[] pastTouchFood;
        try {
            pastTouchFood = worm.getTouchMapFood().get(simulatedTime - worm.getT());
            if (pastTouchFood == null) {
                throw new NullPointerException();
            } else {
                Set<Entry<Long, double[]>> entrySet = worm.getTouchMapFood().entrySet();
                List<Long> remove = new ArrayList<>();
                for (Entry<Long, double[]> entry : entrySet) {
                    if (entry.getKey() < (simulatedTime - worm.getT())) {
                        remove.add(entry.getKey());
                    }
                }
                for (Long l : remove) {
                    worm.getTouchMapFood().remove(l);
                }
            }
        } catch (NullPointerException e) {
            pastTouchFood = new double[2];
            pastTouchFood[0] = 0;
            pastTouchFood[1] = 0;
        }
        return pastTouchFood;
    }

    private double[] getPastTouchWorm(Worm worm) {
        double[] pastTouchWorm;
        try {
            pastTouchWorm = worm.getTouchMapWorm().get(simulatedTime - worm.getT());
            if (pastTouchWorm == null) {
                throw new NullPointerException();
            } else {
                Set<Entry<Long, double[]>> entrySet = worm.getTouchMapWorm().entrySet();
                List<Long> remove = new ArrayList<>();
                for (Entry<Long, double[]> entry : entrySet) {
                    if (entry.getKey() < (simulatedTime - worm.getT())) {
                        remove.add(entry.getKey());
                    }
                }
                for (Long l : remove) {
                    worm.getTouchMapWorm().remove(l);
                }
            }
        } catch (NullPointerException e) {
            pastTouchWorm = new double[2];
            pastTouchWorm[0] = 0;
            pastTouchWorm[1] = 0;
        }
        return pastTouchWorm;
    }

    private double[] getPastNoseValues(Worm worm) {
        double[] pastNose;
        try {
            pastNose = worm.getNoseMap().get(simulatedTime - worm.getT());
            if (pastNose == null) {
                throw new NullPointerException();
            } else {
                Set<Entry<Long, double[]>> entrySet = worm.getNoseMap().entrySet();
                List<Long> remove = new ArrayList<>();
                for (Entry<Long, double[]> entry : entrySet) {
                    if (entry.getKey() < (simulatedTime - worm.getT())) {
                        remove.add(entry.getKey());
                    }
                }
                for (Long l : remove) {
                    worm.getNoseMap().remove(l);
                }
            }

        } catch (NullPointerException e) {
            pastNose = new double[2];
            pastNose[0] = 0;
            pastNose[1] = 0;
        }
        return pastNose;
    }

    private double[] getPastLidarValues(Worm worm, double[] res) {
        double[] pastRes;
        try {
            pastRes = worm.getLidarMap().get(simulatedTime - worm.getT());
            if (pastRes == null) {
                throw new NullPointerException();
            } else {
                Set<Entry<Long, double[]>> entrySet = worm.getLidarMap().entrySet();
                List<Long> remove = new ArrayList<>();
                for (Entry<Long, double[]> entry : entrySet) {
                    if (entry.getKey() < (simulatedTime - worm.getT())) {
                        remove.add(entry.getKey());
                    }
                }
                for (Long l : remove) {
                    worm.getLidarMap().remove(l);
                }
            }
        } catch (NullPointerException e) {
            pastRes = new double[res.length];
            for (int i = 0; i < res.length; i++) {
                pastRes[i] = 0;
            }
        }
        return pastRes;
    }

    private void setTouchSensors(Worm worm) {
        worm.setCurrentTouchWormRight(worm.getTouchWormRight().touchWorm(this.world)[0]);
        worm.setCurrentTouchWormLeft(worm.getTouchWormLeft().touchWorm(this.world)[0]);
        worm.setCurrentTouchFoodRight(worm.getTouchFoodRight().touchFood(this.world)[0]);
        worm.setCurrentTouchFoodLeft(worm.getTouchFoodLeft().touchFood(this.world)[0]);
        worm.setCurrentTouchSelfRight(worm.getTouchSelfRight().touchSelf(this.world)[0]);
        worm.setCurrentTouchSelfLeft(worm.getTouchSelfLeft().touchSelf(this.world)[0]);
    }

    private void setTemperature(Worm worm) {
        for (SimulationBody bodyT : worm.getBodies()) {
            temperature.setMatrixValue((int) Math.round(bodyT.getWorldCenter().y + yWorldDimension / 2),
                    (int) Math.round(bodyT.getWorldCenter().x + xWorldDimension / 2), 150);
        }
    }

    private void updateCurrentTime() {
        double elTime = (double) (System.nanoTime() - startTime) / NANO_TO_BASE;
        int totalSecs = (int) Math.round(elTime);
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        currentTime = hours + ":" + minutes + ":" + seconds;
    }

    private void onClickForFoodAction() {
        System.out.println("food");
        setFoodAfterDeath(pointClicked.x, pointClicked.y);
        String[] toCsv = new String[9];
        toCsv[0] = String.valueOf(simulatedTime);
        toCsv[1] = String.valueOf((double) (System.nanoTime() - startTime) / NANO_TO_BASE);
        toCsv[2] = String.valueOf(0);
        toCsv[3] = String.valueOf(pointClicked.x);
        toCsv[4] = String.valueOf(pointClicked.y);
        toCsv[5] = String.valueOf(-1);
        toCsv[6] = String.valueOf(-1);
        toCsv[7] = String.valueOf(-1);
        toCsv[8] = String.valueOf(-1);
        runCsvTask(toCsv, SimulationControl.fileToSave5, SimulationControl.writer5);
        clickForFood = false;

        int randomFoodItem = random.nextInt(foodCoordinates.size() - 1 - 1) + 1;
        this.world.removeBody(foodCoordinates.get(randomFoodItem));
        foodCoordinates.remove(randomFoodItem);
    }

    private void setWormParameters(double avgTemp, Worm worm) {
        if (Math.abs(worm.getBodies().get(0).getWorldCenter().x - worm.getInitialX()) > worm.getDifferenceX()) {
            worm.setDifferenceX(Math.abs(worm.getBodies().get(0).getWorldCenter().x - worm.getInitialX()));
        }
        if (Math.abs(worm.getBodies().get(0).getWorldCenter().y - worm.getInitialY()) > worm.getDifferenceY()) {
            worm.setDifferenceY(Math.abs(worm.getBodies().get(0).getWorldCenter().y - worm.getInitialY()));
        }
        if (Math.abs(worm.getBodies().get(0).getWorldCenter().x - worm.getRecentInitialX()) > worm
                .getRecentDifferenceX()) {
            worm.setRecentDifferenceX(
                    Math.abs(worm.getBodies().get(0).getWorldCenter().x - worm.getRecentInitialX()));
        }
        if (Math.abs(worm.getBodies().get(0).getWorldCenter().y - worm.getRecentInitialY()) > worm
                .getRecentDifferenceY()) {
            worm.setRecentDifferenceY(
                    Math.abs(worm.getBodies().get(0).getWorldCenter().y - worm.getRecentInitialY()));
        }

        if (Math.abs(worm.getXMax()) < Math.abs(worm.getBodies().get(0).getWorldCenter().x)) {
            worm.setXMax(worm.getBodies().get(0).getWorldCenter().x);
        }
        if (Math.abs(worm.getYMax()) < Math.abs(worm.getBodies().get(0).getWorldCenter().y)) {
            worm.setYMax(worm.getBodies().get(0).getWorldCenter().y);
        }
        if (Math.abs(worm.getRecentXMax()) < Math.abs(worm.getBodies().get(0).getWorldCenter().x)) {
            worm.setRecentXMax(worm.getBodies().get(0).getWorldCenter().x);
        }
        if (Math.abs(worm.getRecentYMax()) < Math.abs(worm.getBodies().get(0).getWorldCenter().y)) {
            worm.setRecentYMax(worm.getBodies().get(0).getWorldCenter().y);
        }
        if (avgTemp >= 6) {
            worm.setHumanPresence(1);
        } else {
            worm.setHumanPresence(0);
        }
    }

    private void fakeHumanOptions() {
        if (this.fakeHuman.equals("fake")) {
            if (presence) {
                int x = SimulationControl.random.nextInt(310 - 160) + 160;
                int y = SimulationControl.random.nextInt(170 - 70) + 70;
                SimulationControl.temperature.setMatrixValue(y, x, 50000);
            }
            counterHuman++;
            counterPause++;
            if (counterHuman == 1000 && presence) {

                // System.out.println("Duration of cycle: " + lastCycleDuration);
                // Random random = new Random();
                switch (fakeHumanBehaviour) {
                    case "bad": {
                        int rnd = SimulationControl.random.nextInt(3);
                        for (int i = 0; i < rnd; i++) {
                            FakeHuman.removeRandomWorm();
                        }

                        break;
                    }
                    case "good": {
                        int rnd = SimulationControl.random.nextInt(3);
                        for (int i = 0; i < rnd; i++) {
                            FakeHuman.feedWorm();
                        }
                        break;
                    }
                    case "badColor": {
                        int rnd = SimulationControl.random.nextInt(3);
                        for (int i = 0; i < rnd; i++) {
                            FakeHuman.removeColoredWorm(5);
                        }
                        break;
                    }
                }
                presence = false;
                counterHuman = 0;
                counterPause = 0;
            }

            if (counterPause == 20000 && !presence) {
                presence = true;
                counterPause = 0;
                counterHuman = 0;
            }
        }
    }

    private void writeLogInfo() {
        for (Worm worm : worms) {
            double elTime = (double) (System.nanoTime() - startTime) / NANO_TO_BASE;
            System.out.print("Gen " + worm.getGeneration() + " | Time: ");
            int totalSecs = (int) Math.round(elTime);
            int hours = totalSecs / 3600;
            int minutes = (totalSecs % 3600) / 60;
            int seconds = totalSecs % 60;
            System.out.printf("%02d:%02d:%02d", hours, minutes, seconds);
            System.out.print(" | " + "Simulated time: " + simulatedTime);
            System.out.println();
        }
        System.out.print("-----------------------------------------------------------");
        System.out.println();
    }

    private void writeDataToCsv() {
        double[] ages = new double[worms.size()];
        double[] differencesX = new double[worms.size()];
        double[] differencesY = new double[worms.size()];
        double[] distances = new double[worms.size()];
        double[] recentDifferencesX = new double[worms.size()];
        double[] recentDifferencesY = new double[worms.size()];
        double[] recentDistances = new double[worms.size()];
        double[] circles = new double[worms.size()];
        double[] colors = new double[worms.size()];
        double[] flagellis = new double[worms.size()];
        double[] flagellisLength = new double[worms.size()];

        for (Worm worm : worms) {
            String[] toCsv = new String[19];
            toCsv[0] = String.valueOf(simulatedTime);
            toCsv[1] = String.valueOf(worm.getAge());
            toCsv[2] = String.valueOf(worm.getInitialX());
            toCsv[3] = String.valueOf(worm.getInitialY());
            toCsv[4] = String.valueOf(worm.getXMax());
            toCsv[5] = String.valueOf(worm.getYMax());
            toCsv[6] = String.valueOf(worm.getDifferenceX());
            toCsv[7] = String.valueOf(worm.getDifferenceY());
            toCsv[8] = String.valueOf(worm.getTotalDistance());
            toCsv[9] = String.valueOf(worm.getRecentDifferenceX());
            toCsv[10] = String.valueOf(worm.getRecentDifferenceY());
            toCsv[11] = String.valueOf(worm.getRecentTotalDistance());
            toCsv[12] = String.valueOf(worm.getCircles());
            toCsv[13] = String.valueOf(Math.round(((Math.tanh(worm.getGenotype()[3]) + 1) / 2) * (9) + 0));
            toCsv[14] = String.valueOf(worm.getFlagelli());
            toCsv[15] = String.valueOf(worm.getFlagelliLength());

            String ge = "";
            for (int s = 0; s < worm.getGenotype().length; s++) {
                if (s == 0) {
                    ge = String.valueOf(worm.getGenotype()[s]);
                } else {
                    ge = ge.concat("/").concat(String.valueOf(worm.getGenotype()[s]));
                }
            }

            toCsv[16] = ge;
            toCsv[17] = String.valueOf(worm.getGeneration());
            toCsv[18] = String.valueOf(worm.getSelectionGeneration());

            ages[worms.indexOf(worm)] = worm.getAge();
            differencesX[worms.indexOf(worm)] = worm.getDifferenceX();
            differencesY[worms.indexOf(worm)] = worm.getDifferenceY();
            distances[worms.indexOf(worm)] = worm.getTotalDistance();
            recentDifferencesX[worms.indexOf(worm)] = worm.getRecentDifferenceX();
            recentDifferencesY[worms.indexOf(worm)] = worm.getRecentDifferenceY();
            recentDistances[worms.indexOf(worm)] = worm.getRecentTotalDistance();
            circles[worms.indexOf(worm)] = worm.getCircles();
            colors[worms.indexOf(worm)] = Math.round(((Math.tanh(worm.getGenotype()[3]) + 1) / 2) * (9) + 0);
            flagellis[worms.indexOf(worm)] = worm.getFlagelli();
            flagellisLength[worms.indexOf(worm)] = worm.getFlagelliLength();

            runCsvTask(toCsv, fileToSave2, writer2);

            worm.setRecentXMax(worm.getBodies().get(0).getWorldCenter().x);
            worm.setRecentYMax(worm.getBodies().get(0).getWorldCenter().y);
            worm.setRecentInitialX(worm.getBodies().get(0).getWorldCenter().x);
            worm.setRecentInitialY(worm.getBodies().get(0).getWorldCenter().y);
            worm.setRecentTotalDistance(0);


        }

        String[] csv = new String[12];
        csv[0] = String.valueOf(simulatedTime);
        csv[1] = String.valueOf(Arrays.stream(ages).average().getAsDouble());
        csv[2] = String.valueOf(Arrays.stream(differencesX).average().getAsDouble());
        csv[3] = String.valueOf(Arrays.stream(differencesY).average().getAsDouble());
        csv[4] = String.valueOf(Arrays.stream(distances).average().getAsDouble());
        csv[5] = String.valueOf(Arrays.stream(recentDifferencesX).average().getAsDouble());
        csv[6] = String.valueOf(Arrays.stream(recentDifferencesY).average().getAsDouble());
        csv[7] = String.valueOf(Arrays.stream(recentDistances).average().getAsDouble());
        csv[8] = String.valueOf(Arrays.stream(circles).average().getAsDouble());
        csv[9] = String.valueOf(Arrays.stream(colors).average().getAsDouble());
        csv[10] = String.valueOf(Arrays.stream(flagellis).average().getAsDouble());
        csv[11] = String.valueOf(Arrays.stream(flagellisLength).average().getAsDouble());

        runCsvTask(csv, fileToSave, writer);
    }

}
